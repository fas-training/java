package com.fas.dao.impl;

import com.fas.config.DbConnect;
import com.fas.dao.StudentDAO;
import com.fas.model.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentDAOImpl implements StudentDAO {

    @Override
    public List<Student> getAllStudents() {
        String sqlQuery = "SELECT * FROM student";
        try (Connection con = DbConnect.getConnection();
             PreparedStatement statement = con.prepareStatement(sqlQuery);
        ) {
            ResultSet rs = statement.executeQuery();

            List<Student> students = new ArrayList<>();

            while (rs.next()) {
                Student student = new Student();

                student.setStudentId(rs.getInt("student_id"));
                student.setFullName(rs.getString("full_name"));
                student.setEmail(rs.getString("email"));
                student.setGpa(rs.getDouble("gpa"));
                student.setGender(rs.getString("gender"));
                student.setDob(rs.getDate("dob").toLocalDate());

                // thêm vào list
                students.add(student);
            }

            return students;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean save(Student student) {
        String sqlQuery = "INSERT INTO student (student_id, full_name, email, gpa, gender, dob) VALUES(?, ?, ?, ?, ?, ?);";
        try (Connection con = DbConnect.getConnection();
            PreparedStatement statement = con.prepareStatement(sqlQuery);
        ) {
            statement.setInt(1, student.getStudentId());
            statement.setString(2, student.getFullName());
            statement.setString(3, student.getEmail());
            statement.setDouble(4, student.getGpa());
            statement.setString(5, student.getGender());
            statement.setDate(6, java.sql.Date.valueOf(student.getDob()));

            int numOfRow = statement.executeUpdate();
            return numOfRow > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean update(Student student) {
        String sqlQuery = "UPDATE student SET full_name=?, email=?, gpa=?, gender=?, dob=? WHERE student_id=?;";
        try (Connection con = DbConnect.getConnection();
             PreparedStatement statement = con.prepareStatement(sqlQuery);
        ) {
            statement.setString(1, student.getFullName());
            statement.setString(2, student.getEmail());
            statement.setDouble(3, student.getGpa());
            statement.setString(4, student.getGender());
            statement.setDate(5, java.sql.Date.valueOf(student.getDob()));
            statement.setInt(6, student.getStudentId());

            int numOfRow = statement.executeUpdate();
            return numOfRow > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(int studentId) {
        String sqlQuery = "DELETE FROM student WHERE student_id=?;";
        try (Connection con = DbConnect.getConnection();
             PreparedStatement statement = con.prepareStatement(sqlQuery);
        ) {
            statement.setInt(1, studentId);

            int numOfRow = statement.executeUpdate();
            return numOfRow > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Student findStudentById(int studentId) {
        String sqlQuery = "SELECT * FROM student WHERE student_id=?;";
        try (Connection con = DbConnect.getConnection();
             PreparedStatement statement = con.prepareStatement(sqlQuery);
        ) {
            statement.setInt(1, studentId);

            ResultSet rs = statement.executeQuery();

            Student student = new Student();

            while (rs.next()) {
                student.setStudentId(rs.getInt("student_id"));
                student.setFullName(rs.getString("full_name"));
                student.setEmail(rs.getString("email"));
                student.setGpa(rs.getDouble("gpa"));
                student.setGender(rs.getString("gender"));
                student.setDob(rs.getDate("dob").toLocalDate());
            }

            return student;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Student> findStudentsWithGpaGreaterThan(double gpaThreshold) {
        String sqlQuery = "SELECT * FROM student WHERE gpa >= ?";
        try (Connection con = DbConnect.getConnection();
             PreparedStatement statement = con.prepareStatement(sqlQuery);
        ) {
            statement.setDouble(1, gpaThreshold);

            ResultSet rs = statement.executeQuery();

            List<Student> students = new ArrayList<>();

            while (rs.next()) {
                Student student = new Student();

                student.setStudentId(rs.getInt("student_id"));
                student.setFullName(rs.getString("full_name"));
                student.setEmail(rs.getString("email"));
                student.setGpa(rs.getDouble("gpa"));
                student.setGender(rs.getString("gender"));
                student.setDob(rs.getDate("dob").toLocalDate());

                // thêm vào list
                students.add(student);
            }

            return students;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
