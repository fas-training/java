package com.fas.dao;

import com.fas.model.Student;

import java.util.List;

public interface StudentDAO {

    // danh sach hoc sinh
    List<Student> getAllStudents();

    // tao moi hoc sinh
    boolean save(Student student);

    // cap nhat hoc sinh
    boolean update(Student student);

    // xoa hoc sinh
    boolean delete(int studentId);

    // tim kiem theo ID
    Student findStudentById(int studentId);

    // danh sach hoc sinh co diem gpa lon hon
    List<Student> findStudentsWithGpaGreaterThan(double gpaThreshold);

}
