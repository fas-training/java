package com.fas;

import com.fas.config.DbConnect;
import com.fas.dao.StudentDAO;
import com.fas.dao.impl.StudentDAOImpl;
import com.fas.model.Student;

import java.sql.*;
import java.time.LocalDate;
import java.util.List;

public class App
{
    public static void main( String[] args ) throws SQLException, ClassNotFoundException {
        StudentDAO studentDAO = new StudentDAOImpl();

        LocalDate localDate = LocalDate.of(2000, 10, 10);
        Student st1 = new Student(3,"min", "min@gmail.com", 9, "O", localDate);
        boolean temp = studentDAO.save(st1);
        System.out.println(temp ? "Them thanh cong" : "that bai");

        st1.setGpa(8);
        st1.setEmail("minmin@gmail.com");
        boolean temp2 = studentDAO.update(st1);
        System.out.println(temp2 ? "cap nhat thanh cong" : "that bai");


        List<Student> studentList = studentDAO.getAllStudents();
        System.out.println("size: " + studentList.size());
        for (Student student : studentList) {
            System.out.println(student);
        }

        double gpaThreshold = 6.0;
        List<Student> studentListWithGpa = studentDAO.findStudentsWithGpaGreaterThan(gpaThreshold);
        System.out.println("size: " + studentListWithGpa.size());
        for (Student student : studentListWithGpa) {
            System.out.println(student);
        }
    }
}
