package com.fas.model;

import java.sql.Date;
import java.time.LocalDate;

public class Student {

    private int studentId;
    private String fullName;
    private String email;
    private double gpa;
    private String gender;
    private LocalDate dob;

    // Constructors
    public Student() {
    }

    public Student(int studentId, String fullName, String email, double gpa, String gender, LocalDate dob) {
        this.studentId = studentId;
        this.fullName = fullName;
        this.email = email;
        this.gpa = gpa;
        this.gender = gender;
        this.dob = dob;
    }

    // Getters and setters
    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        this.gpa = gpa;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentId=" + studentId +
                ", fullName='" + fullName + '\'' +
                ", email='" + email + '\'' +
                ", gpa=" + gpa +
                ", gender='" + gender + '\'' +
                ", dob=" + dob +
                '}';
    }

}
