package com.fas.util;

public class PrintName {

    public String sayHello(String name) {
        if (name == null) {
            throw new NullPointerException("name is null");
        }

        return "Hello, " + name.toUpperCase() + "!";
    }

}
