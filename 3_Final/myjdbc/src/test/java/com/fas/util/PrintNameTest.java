package com.fas.util;

import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.List;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PrintNameTest {

    List<String> listData = null;

//    @BeforeEach
//    public void initData() {
//        listData = new ArrayList<>();
//
//        System.out.println("init data");
//    }
//
//    @AfterEach
//    public void clearData() {
//        System.out.println("clear data");
//    }

    @Test
    @Order(2)
    @DisplayName("test so 1")
    public void testIsValid() {
        System.out.println("test so 1");
        PrintName printName = new PrintName();

        String result = printName.sayHello("fas");

        Assertions.assertEquals("Hello, FAS!", result);
    }

    @Test
    @Order(1)
//    @Disabled
    @DisplayName("test so 2")
    public void testIsInValid() {
        System.out.println("test so 2");
        PrintName printName = new PrintName();

        NullPointerException exception = Assertions.assertThrows(
                NullPointerException.class, () -> printName.sayHello(null));

        Assertions.assertEquals("name is null", exception.getMessage());
    }

}
