package exam;

import java.util.ArrayList;
import java.util.List;

public class ArrayListExam {

    public static void main(String[] args) {
        // Khởi tạo và thêm phần tử vào ArrayList
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Java");
        arrayList.add("Python");
        arrayList.add("C++");

        // Chuyển ArrayList thành mảng
        String[] array = arrayList.toArray(new String[0]);

        // In các phần tử trong mảng
        System.out.println("Các phần tử trong mảng:");
        for (String element : array) {
            System.out.println(element);
        }

        // Lặp qua các phần tử trong mảng
        for (String str : arrayList) {
            System.out.println(str);
        }

        // Sử dụng subList để tạo ra một phần của danh sách
        List<String> subList = arrayList.subList(1, 3);

        System.out.println(subList); // [Python, C++]
    }

}
