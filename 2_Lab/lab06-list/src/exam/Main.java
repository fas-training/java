package exam;


import java.util.List;

public class Main {

    public static void main(String[] args) {
        Author author1 = new Author(101, "Duong");

        Book book1 = new Book(1, "Book1", author1);
        Book book2 = new Book(2, "Book2", author1);

        BookManager bookManager = new BookManager();
        bookManager.addBook(book1);
        bookManager.addBook(book2);

        List<Book> myList =  bookManager.findBooksByAuthor("duong");
        System.out.println(myList);

//        System.out.println(BookManager.bookList.size());
//
//        System.out.println(book1.getAuthor());
    }

}
