package exam;

import java.util.LinkedList;

public class LinkedListExam {

    public static void main(String[] args) {
        LinkedList<String> myLinkedList = new LinkedList<>();

        String secondElement = myLinkedList.get(1); // Accessing elements by index
        System.out.println("secondElement: "+ secondElement);

        myLinkedList.addFirst("Java"); // Adds "Java" at the beginning myLinkedList.addLast("PHP"); // Adds "PHP" at the end
        myLinkedList.removeFirst(); // Removes the first element myLinkedList.removeLast(); // Removes the last element

        for (String s : myLinkedList) { // Iterating through the LinkedList System.out.println(fruit);
            System.out.println(s);
        }
    }

}
