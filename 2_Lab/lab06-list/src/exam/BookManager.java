package exam;

import java.util.ArrayList;
import java.util.List;

public class BookManager {

    static ArrayList<Book> bookList = new ArrayList<>();


    public void addBook(Book book) {
        bookList.add(book);
    }

    public void addBook(int id, String title, Author author) {
        Book book = new Book(id, title, author);
        bookList.add(book);
    }

    public List<Book> findBooksByAuthor(String author) {
        List<Book> books = new ArrayList<>();

        if (bookList != null) {
            for (Book b : bookList) {
                if (b.getAuthor().getName().equalsIgnoreCase(author)) {
                    books.add(b);
                }
            }
        }

        return books;
    }



}
