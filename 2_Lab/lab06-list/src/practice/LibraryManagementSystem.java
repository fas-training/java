package practice;

import java.util.List;

public class LibraryManagementSystem {

    public static void main(String[] args) {
        // Khởi tạo thư viện
        Library library = new Library();

        // Thêm sách và tác giả vào thư viện
        Author author1 = new Author(1, "Author1");
        Author author2 = new Author(2, "Author2");

        Book book1 = new Book(101, "Book1", author1);
        Book book2 = new Book(102, "Book2", author2);

        library.addBook(book1);
        library.addBook(book2);

        // Hiển thị thông tin về tất cả các sách trong thư viện
        System.out.println("Danh sách tất cả các sách trong thư viện:");
        library.displayBooks();

        // Cập nhật thông tin cuốn sách có ID 101
        library.updateBook(101, "UpdatedBook1", author1);
        System.out.println("\nDanh sách sau khi cập nhật:");
        library.displayBooks();

        // Xóa sách có ID 102
        library.deleteBook(102);
        System.out.println("\nDanh sách sau khi xóa:");
        library.displayBooks();

        // Tìm sách theo tác giả "Author1"
        List<Book> booksByAuthor = library.findBooksByAuthor("Author1");
        System.out.println("\nDanh sách sách của tác giả 'Author1':");
        for (Book book : booksByAuthor) {
            System.out.println("ID: " + book.getId() + ", Title: " + book.getTitle() + ", Author: " + book.getAuthor().getName());
        }
    }

}
