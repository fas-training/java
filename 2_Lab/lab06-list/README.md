# List

ArrayList và LinkedList là hai lớp cung cấp cấu trúc dữ liệu danh sách trong Java, nhưng chúng có những đặc điểm khác nhau, và việc chọn lựa giữa chúng phụ thuộc vào yêu cầu cụ thể của ứng dụng.

## So sánh ArrayList và LinkedList

Dưới đây là một số so sánh giữa ArrayList và LinkedList:

**Tốc độ truy cập phần tử**:
- ArrayList có thể truy cập phần tử nhanh hơn vì nó sử dụng mảng để lưu trữ dữ liệu và có thể truy cập trực tiếp vào chỉ số của mảng.
- LinkedList cần phải duyệt qua các liên kết từ đầu đến phần tử cần truy cập, nên truy cập phần tử có thể chậm hơn so với ArrayList.

**Chèn và xóa phần tử**:
- LinkedList thường hiệu quả hơn khi thực hiện các thao tác chèn và xóa vì nó không cần phải sao chép các phần tử như ArrayList. Việc này là do LinkedList chỉ cần cập nhật các liên kết giữa các nút.
- ArrayList có thể chậm hơn khi chèn hoặc xóa ở vị trí trung tâm của danh sách vì cần phải di chuyển các phần tử sau đó để làm cho không gian cho phần tử mới.

**Dung lượng và hiệu suất**:
- ArrayList chiếm ít dung lượng hơn so với LinkedList vì nó chỉ cần lưu trữ mảng và một số chỉ số.
- LinkedList sử dụng nhiều dung lượng hơn do cần lưu thêm thông tin liên kết giữa các nút.

**Iterator và ListIterator**:
- ArrayList hiệu quả hơn khi sử dụng Iterator vì nó có thể truy cập trực tiếp vào chỉ số của mảng.
- LinkedList thường hiệu quả hơn khi sử dụng ListIterator vì nó có thể chuyển đến phần tử trước và sau một phần tử một cách hiệu quả hơn.

Tóm lại, nếu ứng dụng của bạn thường xuyên thực hiện các thao tác chèn và xóa, và không cần truy cập ngẫu nhiên vào các phần tử, thì LinkedList có thể là lựa chọn tốt. Ngược lại, nếu bạn thường xuyên thực hiện các thao tác truy cập ngẫu nhiên hoặc cần hiệu suất cao trong việc đọc dữ liệu, thì ArrayList có thể là lựa chọn tốt.

## Ví dụ

Dưới đây là một ví dụ cụ thể về cách sử dụng ArrayList và LinkedList trong Java:

```
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Iterator;
import java.util.ListIterator;

public class Example {
    public static void main(String[] args) {
        // Sử dụng ArrayList
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Apple");
        arrayList.add("Banana");
        arrayList.add("Orange");

        // Lặp qua các phần tử của ArrayList bằng Iterator
        System.out.println("ArrayList:");
        Iterator<String> arrayIterator = arrayList.iterator();
        while (arrayIterator.hasNext()) {
            System.out.println(arrayIterator.next());
        }

        // Sử dụng LinkedList
        LinkedList<String> linkedList = new LinkedList<>();
        linkedList.add("Red");
        linkedList.add("Green");
        linkedList.add("Blue");

        // Lặp qua các phần tử của LinkedList bằng ListIterator
        System.out.println("\nLinkedList:");
        ListIterator<String> listIterator = linkedList.listIterator();
        while (listIterator.hasNext()) {
            System.out.println(listIterator.next());
        }

        // Thêm một phần tử vào giữa danh sách LinkedList
        linkedList.add(1, "Yellow");

        // Lặp qua danh sách LinkedList theo thứ tự giảm dần
        System.out.println("\nLinkedList (reversed order):");
        Iterator<String> reversedIterator = linkedList.descendingIterator();
        while (reversedIterator.hasNext()) {
            System.out.println(reversedIterator.next());
        }
    }
}
```
