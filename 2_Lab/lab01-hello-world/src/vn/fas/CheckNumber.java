package vn.fas;

import java.util.Scanner;

public class CheckNumber {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Enter an integer: ");
		int number = scanner.nextInt();
		
		// Checking for even or odd and displaying the result
		if (number % 2 == 0) {
			System.out.println(number + " is an even number.");
		} else {
			System.out.println(number + " is an odd number.");
		}
		
		scanner.close();
	}

}
