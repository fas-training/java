package vn.fas;

import java.util.Scanner;

public class StudentInfo {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		// Input name
		System.out.print("Name: ");
		String name = scanner.nextLine();
		
		// Input age
		System.out.print("Age: ");
		int age = scanner.nextInt();
		scanner.nextLine();
		
		// Display input information
		System.out.println("School: Fresher Academy");
		System.out.println("Name: " + name);
		System.out.println("Age: " + age);
		scanner.close();
	}

}
