# JAVA INTRODUCTION

## Nội Dung

- Giới thiệu về Java
- Cài đặt JDK / IDE
- Viết chương trình Java đầu tiên
- Kiểu Dữ Liệu (Data Types)
- Biến và Hằng Số (Variables and Constant)
- Input & Output

## 1. Giới thiệu về Java

### 1.1 Java là gì?

- Java là một ngôn ngữ lập trình và một nền tảng tính toán được phát triển bởi Sun Microsystems (nay là một phần của Oracle Corporation).

![Java là gì](https://1000logos.net/wp-content/uploads/2020/09/Java-Logo.png)

- Java là ngôn ngữ lập trình đa nền tảng (cross-platform), được phát triển bởi James Gosling tại Sun Microsystems (nay là Oracle Corporation). Ngôn ngữ lập trình này ra đời vào năm 1995 và được thiết kế để có thể chạy trên các nền tảng khác nhau, từ máy tính cá nhân đến thiết bị di động, các máy chủ và thiết bị nhúng.

- Java sử dụng cấu trúc lập trình hướng đối tượng (object-oriented programming - OOP) và được xây dựng trên cơ sở của ngôn ngữ lập trình C++. Nó cung cấp một môi trường chạy ảo (virtual machine) gọi là Java Virtual Machine (JVM), giúp các chương trình Java có thể chạy trên nhiều nền tảng khác nhau mà không cần phải biên dịch lại.

### 1.2 Phiên bản Java

Java có nhiều phiên bản khác nhau, mỗi phiên bản thường đi kèm với các cải tiến, sửa lỗi và tính năng mới.

- **Java 1.x**: Là phiên bản ban đầu của Java, bao gồm Java 1.0 và Java 1.1. Đây là thời kỳ khởi đầu của Java và định hình nền tảng cho các phiên bản sau này.

- **Java 2 (J2SE)**: Java 2 Standard Edition bao gồm các phiên bản từ Java 2 Platform, Standard Edition 1.2 (J2SE 1.2) đến J2SE 5.0. Nó giới thiệu nhiều tính năng mới như Swing GUI toolkit, collections framework, và nhiều cải tiến khác.

- **Java SE 6**: Cũng được biết đến với tên gọi là Java 6, đây là một phiên bản quan trọng với nhiều cải tiến như hỗ trợ các tính năng mới trong ngôn ngữ Java và cải thiện hiệu suất.

- **Java SE 7, 8, 9, 10, 11, 12, 13, 14, 15, 16**: Các phiên bản tiếp theo của Java SE tiếp tục mang lại các cải tiến về ngôn ngữ và nền tảng, bao gồm các tính năng như lambda expressions, modular programming (trong Java 9), và một loạt các cải tiến khác.

- **Java SE 17 (LTS)**: Đây là một phiên bản dài hạn được Oracle gán nhãn LTS (Long-Term Support), nhằm cung cấp hỗ trợ kéo dài cho các tổ chức và cá nhân triển khai ứng dụng Java. Đây cũng là phiên bản mà các phiên bản sau của Java SE sẽ dựa trên.

- **Java EE (Enterprise Edition)**: Cung cấp một nền tảng để phát triển và triển khai các ứng dụng doanh nghiệp Java. Các phiên bản của Java EE bao gồm nhiều cải tiến và cập nhật để hỗ trợ các kịch bản phát triển ứng dụng doanh nghiệp.

- **Java ME (Micro Edition)**: Được thiết kế cho các thiết bị có tài nguyên hạn chế như điện thoại di động, máy tính bảng và thiết bị nhúng.

**Lưu Ý**: Nên sử dụng các phiên bản hỗ trợ dài hạn (LTS) cho, bao gồm cập nhật và vá bảo mật. Các phiên bản khác có thể có hỗ trợ hạn chế hoặc không có hỗ trợ.

### 1.3 Tính năng của Java

Dưới đây là một số tính năng quan trọng của Java:

- **Đa nền tảng (Platform Independence)**: Mã nguồn Java được biên dịch thành bytecode, một loại mã trung gian, thay vì mã máy cụ thể của một nền tảng cụ thể. Bytecode có thể chạy trên bất kỳ nền tảng nào có máy ảo Java (JVM).

- **Hướng đối tượng (OOP)**: Java là một Ngôn ngữ Lập trình Hướng đối tượng, điều này có nghĩa là
trong Java mọi thứ được viết dưới dạng các lớp và đối tượng.

- **Bảo Mật**: Java có khả năng bảo mật tốt với các tính năng như xác minh bytecode và
một quản lý bảo mật mạnh mẽ giúp tạo ra các ứng dụng an toàn, lý do nó được sử dụng khá phổ biến.

- **Đa luồng**: Thread là một tiến trình nhẹ và độc lập của một chương trình đang chạy
chia sẻ tài nguyên. Và khi nhiều Thread chạy đồng thời thì được gọi là đa luồng (multithreading).

- **Hệ Sinh Thái**: Sự phong phú và đa dạng của hệ sinh thái Java giúp các nhà phát triển xây dựng các ứng dụng mạnh mẽ, có khả năng mở rộng và hiệu suất cao trên nhiều lĩnh vực khác nhau, từ các hệ thống web và doanh nghiệp đến các giải pháp di động và đám mây, như Spring, Hibernate...

- **Cộng Đồng Phát Triển**: Ngôn ngữ lập trình Java được hỗ trợ bởi một cộng đồng mạnh mẽ đóng
góp vào sự phát triển, sáng tạo và hỗ trợ của nó (Stack Overflow, Reddit và các diễn đàn về Java...)

### 1.4 JDK, JRE và JVM

#### 1.4.1 JDK (Java Development Kit)

- JDK viết tắt của Java Development Kit.
- JDK là một bộ công cụ phát triển phần mềm bao gồm tất cả những gì cần thiết để phát triển ứng dụng Java. Nó bao gồm các công cụ như trình biên dịch Java (javac), runtime Java (JRE), một loạt thư viện và các công cụ phát triển (bộ gỡ lỗi, các công cụ đóng gói...).
- Nhà phát triển sử dụng JDK để viết, biên dịch và gỡ lỗi mã Java.

#### 1.4.2 JRE (Java Runtime Environment)

- JRE viết tắt của Java Runtime Environment.
- JRE là một bộ phần mềm cung cấp môi trường thực thi cần thiết cho các ứng dụng Java chạy.
- JRE là một gói phần mềm bao gồm tất cả những gì cần thiết để chạy các ứng dụng Java. Nó bao gồm Java Virtual Machine (JVM), thư viện cốt lõi và các thành phần cần thiết khác để thực thi các chương trình Java.
- Nếu bạn muốn chạy các ứng dụng Java trên hệ thống của bạn, bạn sẽ cần cài đặt JRE.

#### 1.4.3 JVM (Java Virtual Machine)

- JVM viết tắt của Java Virtual Machine. 
- JVM là một máy ảo cung cấp môi trường thực thi trong đó bytecode Java có thể được thực thi.
- JVM làm cho Java độc lập với nền tảng bằng cách cho phép các chương trình Java chạy trên bất kỳ thiết bị hoặc hệ điều hành nào có JVM tương thích được cài đặt.

## 2. Cài đặt JDK / IDE

### 2.1 Cài đặt JDK

- OpenJDK: https://jdk.java.net/archive/
- Java Oracle: https://www.oracle.com/java/technologies/downloads/

Xem thêm:
- Thiết lập biến môi trường: https://www.youtube.com/watch?v=RqIyua9BFQY

### 2.2 Cài đặt IDE (Integrated Development Environment)

- Eclipse IDE: https://www.eclipse.org/downloads/packages/
- Apache NetBeans: https://netbeans.apache.org/front/main/download/
- IntelliJ IDEA: https://www.jetbrains.com/idea/download/?section=windows

## 3. Viết chương trình Java đầu tiên

Để tạo và chạy một chương trình Java "Hello World" trong Eclipse, bạn có thể làm theo các bước sau:

**Bước 1**: Tải và cài đặt JDK, Eclipse.

**Bước 2**: Tạo một Project mới
- Mở Eclipse
- Chọn `File > New > Java Project`
- Trong hộp thoại `New Java Project`, nhập tên cho dự án của bạn, ví dụ: `HelloWorldProject`
- Nhấp `Finish`

![Hello World](https://www.codejava.net/images/articles/ides/eclipse/java-beginner/New_Java_Project_Dialog.png)

**Bước 3**: Tạo một Package mới
- Trong Package Explorer, nhấp chuột phải vào dự án `HelloWorldProject` vừa tạo
- Chọn `New > Package`
- Trong hộp thoại New Java Package, nhập tên package, ví dụ: `org.example`
- Nhấp `Finish`

![Hello World](https://www.codejava.net/images/articles/ides/eclipse/java-beginner/Menu_New_Package.png)

**Bước 3**: Tạo một Class mới
- Trong Package Explorer, nhấp chuột phải vào package `org.example` vừa tạo. Chọn `New > Class`
- Trong hộp thoại New Java Class, nhập tên class, ví dụ: `Main`
Đảm bảo tùy chọn public static void main(String[] args) được chọn để tạo phương thức main.
- Nhấp `Finish`

![Hello World](https://www.codejava.net/images/articles/ides/eclipse/java-beginner/Menu_New_Class.png)

![Hello World](https://www.codejava.net/images/articles/ides/eclipse/java-beginner/New_Java_Class.png)

**Bước 4**:Viết mã Java "Hello World"
- Eclipse sẽ mở tệp `Main.java` trong trình soạn thảo mã. Bạn sẽ thấy phương thức main đã được tạo tự động
- Bạn chỉ cần thêm dòng `System.out.println("Hello, World!");` vào phương thức main

```java
package org.example;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello, World!");
    }
}
```

**Bước 5**: Chạy chương trình
- Nhấp chuột phải vào tệp `Main.java` trong Package Explorer
- Chọn `Run As > Java Application`

![Hello World](https://www.codejava.net/images/articles/ides/eclipse/java-beginner/Run_Hello_World_Program.png)

Ngoài ra, để biên dịch và chạy một tệp Java từ một package cụ thể, bạn cần phải thực hiện một số bước. Hãy giả sử bạn có một tệp Java có tên Main.java nằm trong package org.exam.

**Bước 1**: Biên dịch tệp Java

```
javac org/exam/Main.java
```

**Bước 2**: Chạy chương trình Java

```
java org.exam.Main
```

**Bước 3**: Xem kết quả

Lệnh trên sẽ chạy phương thức main của lớp Main trong package org.exam và bạn sẽ thấy kết quả in ra `Hello, World!`

## 4. Kiểu Dữ Liệu (Data Types)

Các kiểu dữ liệu quy định các kích thước và giá trị khác nhau có thể được lưu trữ trong biến. Có 2 loại kiểu dữ liệu trong Java:
- **Kiểu nguyên thủy**: Các kiểu dữ liệu nguyên thủy bao gồm `boolean`, `char`, `byte`, `short`, `int`, `long`, `float` và `double`
- **Kiểu tham chiếu**: Các kiểu dữ liệu tham chiếu bao gồm `Class`, `Object`, và `Array`.

![Data Types](https://lh6.googleusercontent.com/3Imai32xzL8Q7Rd6Hwh6nyRY6yp0JJFCnUWX29orysd05uLa9rBz9--vpg1h4iS3-foi4TBS3WhqfAxQxMvEMJFEeutp9XjpwdGpLFPHhpXlMUqZ1UbZ7wWJhTQG4G76gSdSh0gOSEcESreSVA)

## 5. Biến và Hằng Số (Variables and Constant)

### 5.1 Biến là gì?

- Biến trong lập trình là một định danh được sử dụng để lưu trữ dữ liệu có thể thay đổi trong quá trình thực thi của chương trình.

- Nói cách khác, biến là một vùng trong bộ nhớ được đặt tên để lưu trữ các giá trị như số, chuỗi văn bản, đối tượng và các loại dữ liệu khác.

- Việc khai báo một biến bao gồm việc chỉ định tên và kiểu của nó: `dataType variableName = value;`

Đây là cú pháp khai báo biến:
- **dataType**: Đại diện cho loại dữ liệu mà biến sẽ giữ (ví dụ: int, double, String, boolean)
- **variableName**: Đó là tên được đặt cho biến
- **value**: Gán giá trị ban đầu tại thời điểm khai báo

Dưới đây là bảng mô tả các kiểu dữ liệu nguyên thủy trong Java cùng với các ví dụ minh họa cụ thể cho từng kiểu dữ liệu:

| Kiểu Dữ Liệu  | Kích Thước          | Giá Trị Mặc Định    | Phạm Vi Giá Trị                                      | Ví Dụ         |
|---------------|----------------------|---------------------|------------------------------------------------------|---------------|
| `byte`        | 8-bit                | 0                   | -128 đến 127                                         | `byte b = 10;`|
| `short`       | 16-bit               | 0                   | -32,768 đến 32,767                                   | `short s = 20000;`|
| `int`         | 32-bit               | 0                   | -2^31 đến 2^31-1 (tức là -2,147,483,648 đến 2,147,483,647) | `int i = 100000;`|
| `long`        | 64-bit               | 0L                  | -2^63 đến 2^63-1 (tức là -9,223,372,036,854,775,808 đến 9,223,372,036,854,775,807) | `long l = 100000L;`|
| `float`       | 32-bit               | 0.0f                | Giá trị rất lớn, độ chính xác đơn                    | `float f = 5.75f;`|
| `double`      | 64-bit               | 0.0d                | Giá trị rất lớn, độ chính xác kép                    | `double d = 19.99;`|
| `char`        | 16-bit               | '\u0000' (null)     | '\u0000' đến '\uffff' (0 đến 65,535)                 | `char c = 'A';`|
| `boolean`     | 1-bit (theo lý thuyết)| false              | `true` hoặc `false`                                  | `boolean bool = true;`|

#### Stack và Heap

Trong Java, các kiểu dữ liệu nguyên thủy như byte và int có thể được lưu trữ ở hai nơi chính `stack` hoặc `heap`. Vị trí lưu trữ phụ thuộc vào cách chúng được khai báo và sử dụng trong chương trình. 
- **Stack**: Stack là một vùng bộ nhớ nhỏ nhưng nhanh, dùng để lưu trữ các biến cục bộ và thông tin điều khiển (như địa chỉ trả về, tham số của hàm). Các biến lưu trữ trên stack có phạm vi tồn tại giới hạn trong khối mã mà chúng được khai báo.
- **Heap**: Heap là một vùng bộ nhớ lớn hơn và chậm hơn so với stack, dùng để lưu trữ các đối tượng được khởi tạo động. Các biến được lưu trữ trên heap có thể tồn tại cho đến khi chúng không còn được tham chiếu và được bộ thu gom rác (garbage collection) dọn dẹp.

##### 1. Biến cục bộ (Local Variables)

Biến cục bộ được khai báo bên trong phương thức hoặc khối lệnh. Chúng được lưu trữ trên stack.

```java
public class Example {
    public void method() {
        int localVar = 10; // Biến cục bộ, được lưu trữ trên stack
        byte localByte = 20; // Biến cục bộ, được lưu trữ trên stack
    }
}
```

##### 2. Biến thể hiện (Instance Variables)

Biến thể hiện của một lớp được lưu trữ trên heap cùng với đối tượng chứa chúng.

```java
public class Example {
    private int instanceVar; // Biến thể hiện, được lưu trữ trên heap

    public Example() {
        instanceVar = 10;
    }
}
```

##### 3. Biến tĩnh (Static Variables)

Biến tĩnh thuộc về lớp, không thuộc về bất kỳ đối tượng nào cụ thể. Chúng được lưu trữ trong vùng bộ nhớ đặc biệt dành cho các thành viên tĩnh của lớp, thường được gọi là "metaspace" hoặc "method area".

```java
public class Example {
    private static int staticVar = 10; // Biến tĩnh, được lưu trữ trong method area

    public static void main(String[] args) {
        // ...
    }
}
```

**Tóm tắt**:
- Biến cục bộ (Local Variables): Được lưu trữ trên stack. Chúng tồn tại trong thời gian thực thi của phương thức.
- Biến thể hiện (Instance variables): Được lưu trữ trên heap. Chúng tồn tại trong suốt vòng đời của đối tượng chứa chúng.
- Biến tĩnh (Static variables): Được lưu trữ trong vùng bộ nhớ đặc biệt (method area).

### 5.2 Hằng Số là gì?

- Hằng số đề cập đến một biến mà giá trị không thể thay đổi sau khi đã được gán một lần. Hằng số được khai báo bằng từ khóa `final`

- Sau khi khởi tạo, giá trị của hằng số được giữ cố định suốt quá trình thực thi chương trình. Thông thường, hằng số được sử dụng để lưu trữ các giá trị không được sửa đổi

Ví Dụ:

```java
public class JavaExample {
    // Declaration of constant variables
    public final int MAX_VALUE = 100;
    public static final double PI = 3.14159;
    public static final String APPLICATION_NAME = "MyApp";
}
```

## 6. Input & Output

- Input và Output trong Java cho phép bạn tương tác với người dùng thông qua các thiết bị đầu vào (như bàn phím) và thiết bị đầu ra (như màn hình hoặc bảng điều khiển).

### 6.1 Input là gì?

- Java cung cấp các cách khác nhau để nhận đầu vào từ người dùng. Tuy nhiên, chúng ta sẽ học cách nhận đầu vào từ người dùng bằng cách sử dụng đối tượng của lớp `Scanner`

- Trước khi sử dụng, chúng ta cần import gói `java.util.Scanner`

| Phương thức    | Mô tả                                  |
|----------------|----------------------------------------|
| `nextLine()`   | Đọc một giá trị chuỗi từ người dùng    |
| `nextInt()`    | Đọc giá trị `int` từ người dùng        |
| `nextBoolean()`| Đọc giá trị `boolean` từ người dùng    |
| `nextDouble()` | Đọc giá trị `double` từ người dùng     |
| `nextByte()`   | Đọc giá trị `byte` từ người dùng       |

Ví Dụ:

```java
import java.util.Scanner;

public class ScannerExample {
    public static void main(String[] args) {
        // Tạo một đối tượng Scanner để đọc đầu vào từ người dùng
        Scanner scanner = new Scanner(System.in);

        // Đọc một chuỗi từ người dùng
        System.out.print("Nhập một chuỗi: ");
        String str = scanner.nextLine();
        System.out.println("Bạn đã nhập chuỗi: " + str);

        // Đọc một số nguyên từ người dùng
        System.out.print("Nhập một số nguyên: ");
        int intValue = scanner.nextInt();
        System.out.println("Bạn đã nhập số nguyên: " + intValue);

        // Đọc một giá trị boolean từ người dùng
        System.out.print("Nhập một giá trị boolean (true/false): ");
        boolean boolValue = scanner.nextBoolean();
        System.out.println("Bạn đã nhập giá trị boolean: " + boolValue);

        // Đọc một số thực từ người dùng
        System.out.print("Nhập một số thực: ");
        double doubleValue = scanner.nextDouble();
        System.out.println("Bạn đã nhập số thực: " + doubleValue);

        // Đọc một giá trị byte từ người dùng
        System.out.print("Nhập một giá trị byte: ");
        byte byteValue = scanner.nextByte();
        System.out.println("Bạn đã nhập giá trị byte: " + byteValue);

        // Đóng đối tượng Scanner để giải phóng tài nguyên
        scanner.close();
    }
}
```

### 6.2 Output là gì?

Trong Java, bạn có thể xuất dữ liệu ra màn hình bằng cách sử dụng các phương thức của lớp `System.out`. Dưới đây là một số phương thức phổ biến để xuất dữ liệu:

#### System.out.print()

Phương thức này in ra văn bản được chỉ định nhưng không thêm một dòng mới sau đó. Các lần in tiếp theo sẽ tiếp tục trên cùng một dòng.

Ví Dụ:

```java
public class PrintExample {
    public static void main(String[] args) {
        System.out.print("Hello");
        System.out.print(" ");
        System.out.print("World!");
    }
}
```

Kết Quả:

```
Hello World!
```

#### System.out.println()

Phương thức này in ra văn bản được chỉ định và sau đó thêm một dòng mới. Các lần in tiếp theo sẽ bắt đầu từ dòng mới.

Ví Dụ:

```java
public class PrintlnExample {
    public static void main(String[] args) {
        System.out.println("Hello");
        System.out.println("World!");
    }
}
```

Kết Quả:

```
Hello
World!
```

#### System.out.printf()

Phương thức này cung cấp cách in định dạng (format), cho phép bạn chèn các giá trị vào văn bản định dạng bằng các trình giữ chỗ (format specifiers).

Ví Dụ:

```java
public class PrintfExample {
    public static void main(String[] args) {
        int year = 2024;
        double temperature = 23.5;
        String city = "Hanoi";

        System.out.printf("In %d, the temperature in %s is %.1f degrees Celsius.\n", year, city, temperature);
    }
}
```

Kết Quả:

```
In 2024, the temperature in Hanoi is 23.5 degrees Celsius.
```

| Trình Giữ Chỗ | Kiểu Dữ Liệu           | Mô Tả                         |
|---------------|------------------------|-------------------------------|
| `%d`          | Số nguyên (int)        | In ra một số nguyên           |
| `%f`          | Số thực (float, double)| In ra một số thực             |
| `%s`          | Chuỗi (String)         | In ra một chuỗi               |
| `%c`          | Ký tự (char)           | In ra một ký tự               |
| `%b`          | Giá trị boolean (boolean) | In ra giá trị boolean         |

=== THE END ===
