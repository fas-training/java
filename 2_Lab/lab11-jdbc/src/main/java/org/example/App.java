package org.example;

import java.sql.*;

public class App
{
    public static void main(String[] args) {
        String baseUrl = "jdbc:mysql://localhost:3306/dbuniversity";
        String username = "root";
        String password = "12345678";
        try (Connection connection = DriverManager.getConnection(baseUrl, username, password)) {
            System.out.println(connection.getMetaData());

            Statement statement = connection.createStatement();

            String sql = "select * from student";
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                System.out.println(resultSet.getString(1));
                System.out.println(resultSet.getString(2));
            }

            resultSet.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();;
        }
    }
}
