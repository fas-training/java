-- Create the category table
CREATE TABLE IF NOT EXISTS category (
     category_id INT PRIMARY KEY AUTO_INCREMENT,
     category_name VARCHAR(255) NOT NULL
);

-- Create the product table
CREATE TABLE IF NOT EXISTS product (
    product_id INT PRIMARY KEY AUTO_INCREMENT,
    product_name VARCHAR(255) NOT NULL,
    price DECIMAL(10, 2) NOT NULL,
    status BOOLEAN NOT NULL DEFAULT false,
    category_id INT,
    FOREIGN KEY (category_id) REFERENCES category(category_id)
);
