package org.example.practice2.model;

public class Product {

    private int productId;
    private String productName;
    private double price;
    private boolean status;
    private int categoryId;

    public Product() {
    }

    public Product(int productId, String productName, double price, boolean status, int categoryId) {
        this.productId = productId;
        this.productName = productName;
        this.price = price;
        this.status = status;
        this.categoryId = categoryId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

}
