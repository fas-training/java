package org.example.practice2;

import org.example.practice2.config.DbConnection;
import org.example.practice2.dao.CategoryDAO;
import org.example.practice2.model.Category;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class MainApp {

    public static void main(String[] args) {
        // Thực hiện kết nối đến cơ sở dữ liệu (ví dụ: MySQL)
        try {
            Connection connection = DbConnection.getConnection();

            // Tạo CategoryDAO với kết nối
            CategoryDAO categoryDAO = new CategoryDAO(connection);

            // Thêm mới một category
            Category newCategory = new Category();
            newCategory.setCategoryName("Electronics");
            categoryDAO.createCategory(newCategory);

            System.out.println("Category ID: " + newCategory.getCategoryId());

            // Hiển thị tất cả các categories
            List<Category> allCategories = categoryDAO.getAllCategories();
            for (Category category : allCategories) {
                System.out.println("Category ID: " + category.getCategoryId() + ", Category Name: " + category.getCategoryName());
            }

            // Cập nhật một category
            Category categoryToUpdate = allCategories.get(0);
            categoryToUpdate.setCategoryName("Updated Electronics");
            categoryDAO.updateCategory(categoryToUpdate);

            // Hiển thị lại danh sách categories sau khi cập nhật
            List<Category> updatedCategories = categoryDAO.getAllCategories();
            for (Category category : updatedCategories) {
                System.out.println("Category ID: " + category.getCategoryId() + ", Category Name: " + category.getCategoryName());
            }

            // Xóa một category (chỉ để minh họa, hãy chắc chắn rằng category này không có product liên quan)
            int categoryIdToDelete = updatedCategories.get(0).getCategoryId();
            categoryDAO.deleteCategory(categoryIdToDelete);

            // Hiển thị lại danh sách categories sau khi xóa
            List<Category> remainingCategories = categoryDAO.getAllCategories();
            for (Category category : remainingCategories) {
                System.out.println("Category ID: " + category.getCategoryId() + ", Category Name: " + category.getCategoryName());
            }

            // Đóng kết nối
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
