package org.example.practice2.dao;

import org.example.practice2.model.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductDAO {

    private Connection connection;

    public ProductDAO(Connection connection) {
        this.connection = connection;
    }

    // Create a new product
    public void createProduct(Product product) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(
                "INSERT INTO product (product_name, price, status, category_id) VALUES (?, ?, ?, ?)",
                Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, product.getProductName());
            preparedStatement.setDouble(2, product.getPrice());
            preparedStatement.setBoolean(3, product.isStatus());
            preparedStatement.setInt(4, product.getCategoryId());
            preparedStatement.executeUpdate();

            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                product.setProductId(generatedKeys.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Read all products
    public List<Product> getAllProducts() {
        List<Product> products = new ArrayList<>();
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM product")) {

            while (resultSet.next()) {
                Product product = new Product();
                product.setProductId(resultSet.getInt("product_id"));
                product.setProductName(resultSet.getString("product_name"));
                product.setPrice(resultSet.getDouble("price"));
                product.setStatus(resultSet.getBoolean("status"));
                product.setCategoryId(resultSet.getInt("category_id"));
                products.add(product);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return products;
    }

    // Update a product
    public void updateProduct(Product product) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(
                "UPDATE product SET product_name = ?, price = ?, status = ?, category_id = ? WHERE product_id = ?")) {
            preparedStatement.setString(1, product.getProductName());
            preparedStatement.setDouble(2, product.getPrice());
            preparedStatement.setBoolean(3, product.isStatus());
            preparedStatement.setInt(4, product.getCategoryId());
            preparedStatement.setInt(5, product.getProductId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Delete a product
    public void deleteProduct(int productId) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(
                "DELETE FROM product WHERE product_id = ?")) {
            preparedStatement.setInt(1, productId);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
