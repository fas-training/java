package org.example.practice1.model;

public class Student {

    private int studentId;
    private String fullName;
    private String email;
    private double gpa;
    private String gender;

    public Student() {
    }

    public Student(int studentId, String fullName, String email, double gpa, String gender) {
        this.studentId = studentId;
        this.fullName = fullName;
        this.email = email;
        this.gpa = gpa;
        this.gender = gender;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        this.gpa = gpa;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentId=" + studentId +
                ", fullName='" + fullName + '\'' +
                ", email='" + email + '\'' +
                ", gpa=" + gpa +
                ", gender='" + gender + '\'' +
                '}';
    }

}
