package org.example.practice1.dao;

import org.example.practice1.config.DbConnection;
import org.example.practice1.model.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentDAO {

    public void addStudent(Student student) {
        try (Connection connection = DbConnection.getConnection()) {
            String sql = "INSERT INTO student (student_id, full_name, email, gpa, gender) VALUES (?, ?, ?, ?, ?)";

            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setInt(1, student.getStudentId());
                preparedStatement.setString(2, student.getFullName());
                preparedStatement.setString(3, student.getEmail());
                preparedStatement.setDouble(4, student.getGpa());
                preparedStatement.setString(5, student.getGender());

                preparedStatement.executeUpdate();
                System.out.println("Student added successfully.");
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle exceptions appropriately in a real application
            System.out.println("SQLException: " + e.getMessage());
        }
    }

    public List<Student> findAll() {
        List<Student> students = new ArrayList<>();

        try (Connection connection = DbConnection.getConnection()) {
            String sql = "SELECT * FROM student";

            try (PreparedStatement preparedStatement = connection.prepareStatement(sql);
                 ResultSet resultSet = preparedStatement.executeQuery()) {

                while (resultSet.next()) {
                    int studentId = resultSet.getInt("student_id");
                    String fullName = resultSet.getString("full_name");
                    String email = resultSet.getString("email");
                    double gpa = resultSet.getDouble("gpa");
                    String gender = resultSet.getString("gender");

                    Student student = new Student(studentId, fullName, email, gpa, gender);
                    students.add(student);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle exceptions appropriately in a real application
        }

        return students;
    }

    public Student findById(int studentId) {
        try (Connection connection = DbConnection.getConnection()) {
            String sql = "SELECT * FROM student WHERE student_id = ?";

            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setInt(1, studentId);

                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        String fullName = resultSet.getString("full_name");
                        String email = resultSet.getString("email");
                        double gpa = resultSet.getDouble("gpa");
                        String gender = resultSet.getString("gender");

                        return new Student(studentId, fullName, email, gpa, gender);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle exceptions appropriately in a real application
        }

        return null; // Return null if no student found or an error occurred
    }

    public void updateStudentGPA(int studentId, double newGPA) {
        try (Connection connection = DbConnection.getConnection()) {
            String sql = "UPDATE student SET gpa = ? WHERE student_id = ?";

            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setDouble(1, newGPA);
                preparedStatement.setInt(2, studentId);

                int rowsAffected = preparedStatement.executeUpdate();
                if (rowsAffected > 0) {
                    System.out.println("Student GPA updated successfully.");
                } else {
                    System.out.println("Student not found or an error occurred.");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle exceptions appropriately in a real application
        }
    }

    public void deleteStudent(int studentId) {
        try (Connection connection = DbConnection.getConnection()) {
            String sql = "DELETE FROM student WHERE student_id = ?";

            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setInt(1, studentId);

                int rowsAffected = preparedStatement.executeUpdate();
                if (rowsAffected > 0) {
                    System.out.println("Student deleted successfully.");
                } else {
                    System.out.println("Student not found or an error occurred.");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle exceptions appropriately in a real application
        }
    }

    public List<Student> findWithStoredProcedure(String gen) {
        List<Student> students = new ArrayList<>();

        try (Connection connection = DbConnection.getConnection()) {
            String sql = "{call GetStudentByGender(?)}";

            CallableStatement callableStatement = connection.prepareCall(sql);
            callableStatement.setString(1, gen);

            ResultSet resultSet = callableStatement.executeQuery();

            while (resultSet.next()) {
                int studentId = resultSet.getInt("student_id");
                String fullName = resultSet.getString("full_name");
                String email = resultSet.getString("email");
                double gpa = resultSet.getDouble("gpa");
                String gender = resultSet.getString("gender");

                Student student = new Student(studentId, fullName, email, gpa, gender);
                students.add(student);
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle exceptions appropriately in a real application
        }

        return students;
    }

    public String findWithFunction(double gpa) {
        String result = null;

        try (Connection connection = DbConnection.getConnection()) {
            String sql = "{? = call GetResultByGPA(?)}";

            CallableStatement callableStatement = connection.prepareCall(sql);

            callableStatement.registerOutParameter(1, Types.VARCHAR);
            callableStatement.setDouble(2, gpa);

            ResultSet resultSet = callableStatement.executeQuery();

            if (resultSet.next()) {
                result = resultSet.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Handle exceptions appropriately in a real application
        }

        return result;
    }

}
