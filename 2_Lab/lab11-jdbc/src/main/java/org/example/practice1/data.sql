-- Create the university database
CREATE DATABASE IF NOT EXISTS dbuniversity;
USE university;

-- Create the student table
CREATE TABLE IF NOT EXISTS student (
    student_id INT PRIMARY KEY,
    full_name VARCHAR(255),
    email VARCHAR(255) UNIQUE,
    gpa DOUBLE,
    gender VARCHAR(10)
);

-- Create the stored procedure
DELIMITER //
CREATE PROCEDURE GetStudentByGender(IN gen VARCHAR(10)) BEGIN
SELECT *
FROM student s
WHERE s.gender = gen;
END //
DELIMITER ;

-- Create the function
DELIMITER //
CREATE FUNCTION GetResultByGPA(gpa DECIMAL(3,2))
    RETURNS VARCHAR(10)
    READS SQL DATA
    DETERMINISTIC
BEGIN
    DECLARE result VARCHAR(10);

    IF gpa >= 5.0 THEN
        SET result = 'Pass';
ELSE
        SET result = 'Fail';
END IF;

RETURN result;
END //
DELIMITER ;