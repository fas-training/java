package org.example.practice1.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class DbConnection {

    private static final String JDBC_URL = "jdbc:mysql://localhost:3306/dbuniversity";
    private static final String JDBC_USER = "root";
    private static final String JDBC_PASSWORD = "12345678";

    // Private constructor to prevent instantiation
    private DbConnection() {
    }

    // Static block to load the JDBC driver when the class is loaded
    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Error loading MySQL JDBC driver.");
        }
    }

    // Method to get a database connection
    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASSWORD);
    }

}
