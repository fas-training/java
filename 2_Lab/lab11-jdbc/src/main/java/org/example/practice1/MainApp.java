package org.example.practice1;

import org.example.practice1.dao.StudentDAO;
import org.example.practice1.model.Student;

import java.util.List;

public class MainApp
{
    public static void main(String[] args) {
        StudentDAO studentDAO = new StudentDAO();

        // insert
        Student st1 = new Student();
        st1.setStudentId(1);
        st1.setFullName("duong");
        st1.setEmail("duong@gmail.com");
        st1.setGpa(10.0);
        st1.setGender("male");
        studentDAO.addStudent(st1);

        Student student = studentDAO.findById(1);
        if (student != null) {
            System.out.println(student.toString());
        }

        // get all
        List<Student> students = studentDAO.findAll();
        if (students != null) {
            for (Student s : students) {
                System.out.println(s.toString());
            }
        }
    }
}
