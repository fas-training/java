# Set

HashSet và TreeSet là hai cài đặt của giao diện Set trong Java, nhưng chúng có những đặc điểm khác nhau trong cách họ duy trì và sắp xếp các phần tử.

## So sánh HashSet và TreeSet

Dưới đây là một số so sánh giữa HashSet và TreeSet:

**Thứ tự của các phần tử**:
- HashSet: Không đảm bảo thứ tự của các phần tử, tức là không giữ thứ tự chèn hoặc bất kỳ thứ tự nào khác.
- TreeSet: Sắp xếp các phần tử theo thứ tự tăng dần (hoặc giảm dần nếu bạn cung cấp một Comparator). Bảo đảm rằng bạn sẽ nhận được các phần tử theo thứ tự mong muốn.

**Dùng để**:
- HashSet: Thích hợp khi bạn cần duy trì một tập hợp các phần tử mà không quan trọng đến thứ tự.
- TreeSet: Hữu ích khi bạn cần duy trì một tập hợp được sắp xếp và cần thực hiện các thao tác dựa trên thứ tự.

**Null value**:
- HashSet: Cho phép giá trị null.
- TreeSet: Không chấp nhận giá trị null.

## Ví dụ

Dưới đây là một ví dụ minh họa sử dụng HashSet và TreeSet trong Java, trong đó TreeSet được sắp xếp giảm dần:

```
import java.util.HashSet;
import java.util.TreeSet;
import java.util.Set;

public class SetExample {
    public static void main(String[] args) {
        // Sử dụng HashSet
        Set<String> hashSet = new HashSet<>();
        hashSet.add("Banana");
        hashSet.add("Apple");
        hashSet.add("Orange");

        // Hiển thị các phần tử của HashSet (không có thứ tự đặc biệt)
        System.out.println("HashSet:");
        for (String element : hashSet) {
            System.out.println(element);
        }

        // Sử dụng TreeSet với Comparator.reverseOrder() để sắp xếp giảm dần
        Set<String> treeSet = new TreeSet<>(java.util.Collections.reverseOrder());
        treeSet.addAll(hashSet);

        // Hiển thị các phần tử của TreeSet (sắp xếp giảm dần)
        System.out.println("\nTreeSet (descending order):");
        for (String element : treeSet) {
            System.out.println(element);
        }
    }
}
```
