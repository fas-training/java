package exam;

import java.util.HashSet;

public class HashSetExample {

    public static void main(String[] args) {
        // Tạo một HashSet để lưu trữ các số nguyên
        HashSet<Integer> hashSet = new HashSet<>();

        // Thêm phần tử vào HashSet
        hashSet.add(10);
        hashSet.add(40);
        hashSet.add(30);
        hashSet.add(20);
        hashSet.add(50);

        // Duyệt qua các phần tử trong HashSet và hiển thị chúng
        System.out.println("Các phần tử trong HashSet:");
        for (Integer element : hashSet) {
            System.out.println(element);
        }

        // Kiểm tra xem một phần tử có tồn tại trong HashSet hay không
        int targetElement = 30;
        if (hashSet.contains(targetElement)) {
            System.out.println(targetElement + " tồn tại trong HashSet");
        } else {
            System.out.println(targetElement + " không tồn tại trong HashSet");
        }

        // Loại bỏ một phần tử khỏi HashSet
        int elementToRemove = 40;
        hashSet.remove(elementToRemove);
        System.out.println("HashSet sau khi loại bỏ " + elementToRemove + ": " + hashSet);
    }

}
