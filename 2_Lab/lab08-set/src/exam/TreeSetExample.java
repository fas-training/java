package exam;

import java.util.Comparator;
import java.util.TreeSet;

public class TreeSetExample {

    public static void main(String[] args) {
        // Sử dụng TreeSet với Comparator.reverseOrder() để sắp xếp giảm dần
        TreeSet<Integer> treeSet = new TreeSet<>(Comparator.reverseOrder());

        // Thêm phần tử vào TreeSet
        treeSet.add(10);
        treeSet.add(40);
        treeSet.add(30);
        treeSet.add(20);
        treeSet.add(50);

        // Duyệt qua các phần tử trong TreeSet và hiển thị chúng
        System.out.println("Các phần tử trong TreeSet:");
        for (Integer element : treeSet) {
            System.out.println(element);
        }

        // Kiểm tra xem một phần tử có tồn tại trong TreeSet hay không
        Integer targetElement = 10;
        if (treeSet.contains(targetElement)) {
            System.out.println(targetElement + " tồn tại trong TreeSet");
        } else {
            System.out.println(targetElement + " không tồn tại trong TreeSet");
        }

        // Loại bỏ một phần tử khỏi TreeSet
        Integer elementToRemove = 50;
        treeSet.remove(elementToRemove);
        System.out.println("TreeSet sau khi loại bỏ " + elementToRemove + ": " + treeSet);
    }

}