package model;

// Lớp Student kế thừa từ Person
public class Student extends Person {

    // Thuộc tính riêng của Student
    private double allowance;

    // Constructor
    public Student(int id, String name, int age, double allowance) {
        super(id, name, age); // Gọi constructor của lớp cơ sở
        this.allowance = allowance;
    }

    // Getter và setter cho allowance
    public double getAllowance() {
        return allowance;
    }

    public void setAllowance(double allowance) {
        this.allowance = allowance;
    }

    // Override phương thức hiển thị thông tin
    @Override
    public void displayInfo() {
        super.displayInfo(); // Gọi phương thức của lớp cơ sở
        System.out.println("Allowance: " + allowance);
    }

}
