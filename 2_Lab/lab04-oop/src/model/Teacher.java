package model;

// Lớp Teacher kế thừa từ Person
public class Teacher extends Person {

    // Thuộc tính riêng của Teacher
    private String level; // A, B, hoặc C
    private double salary;

    // Constructor
    public Teacher(int id, String name, int age, String level, double salary) {
        super(id, name, age); // Gọi constructor của lớp cơ sở
        this.level = level;
        this.salary = salary;
    }

    // Getter và setter cho salary
    public double getSalary() {
        double multiplier = 1.0;
        switch (level) {
            case "A":
                multiplier = 1.5;
                break;
            case "B":
                multiplier = 1.2;
                break;
            case "C":
                multiplier = 1.0;
                break;
        }
        return salary * multiplier;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    // Override phương thức hiển thị thông tin
    @Override
    public void displayInfo() {
        super.displayInfo(); // Gọi phương thức của lớp cơ sở
        System.out.println("Level: " + level);
        System.out.println("Salary: " + salary);
    }

}
