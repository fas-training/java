package main;

import model.Student;
import model.Teacher;

// Lớp chính để chạy chương trình
public class Main {
    public static void main(String[] args) {
        // Tạo đối tượng Student
        Student student1 = new Student(1, "Duong Truong", 20, 1500.00);
        // Tạo đối tượng Teacher
        Teacher teacher1 = new Teacher(2, "Min Nguyen", 35, "A", 8000.00);

        // Hiển thị thông tin Student
        student1.displayInfo();
        System.out.println();

        // Hiển thị thông tin Teacher
        teacher1.displayInfo();
    }
}
