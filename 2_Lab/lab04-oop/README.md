# JAVA OOP (Object-Oriented Programming)

## Nội Dung

- OOP là gì
- Tính đóng gói (Encapsulation)
- Tính kế thừa (Inheritance)
- Tính đa hình (Polymorphism)
- Tính trừu tượng (Abstraction)
- Interface và Abstract class

## 1. Giới Thiệu OOP

![OOP](https://www.tutorialspoint.com/java/images/java-oops-concepts.jpg)

### 1.1 OOP là gì?

Lập trình hướng đối tượng (OOP) trong Java là một phương pháp lập trình mạnh mẽ và linh hoạt, cho phép bạn tổ chức mã nguồn của mình thành các đối tượng có ý nghĩa, giúp dễ dàng quản lý và tái sử dụng mã nguồn.

OPP giúp giải quyết các vấn đề phức tạp trong lập trình bằng cách chia các vấn đề thành các đối tượng nhỏ hơn và dễ quản lý hơn.

### 1.2 Ưu điểm OOP?

- **Tính tái sử dụng (Reusability)**: Các đối tượng có thể được sử dụng lại trong các phần của chương trình hoặc trong các chương trình khác mà không cần phải viết lại mã.
- **Tính tổ chức (Organization)**: OOP giúp tổ chức mã nguồn một cách logic và cấu trúc hóa, giúp dễ dàng quản lý và bảo trì.
- **Tính mở rộng (Extensibility)**: Bằng cách sử dụng kế thừa và ghi đè, bạn có thể mở rộng và mở rộng chức năng của các lớp đã có mà không cần phải sửa đổi mã nguồn gốc.
- **Đóng gói (Encapsulation)**: OOP cho phép đóng gói dữ liệu và phương thức liên quan vào trong một đối tượng, bảo vệ dữ liệu khỏi sự truy cập không cần thiết từ bên ngoài và tăng tính bảo mật của chương trình.
- **Đa hình (Polymorphism)**: Khả năng để một phương thức có thể được triển khai khác nhau trong các lớp con, cho phép chương trình sử dụng một giao diện chung cho các đối tượng có cùng loại.
- **Tính trừu tượng (Abstraction)**: OOP cho phép tạo ra các lớp trừu tượng mô tả các tính chất chung của một nhóm đối tượng mà không cần biết chi tiết về cách thức thực hiện.

## 2. Tìm hiểu 4 tính chất OOP

![OOP](https://static-xf1.vietnix.vn/wp-content/uploads/2022/09/4-nguyen-ly-co-ban-cua-oop.webp)

### 2.1 Tính đóng gói (Encapsulation)

Tính đóng gói hay tính bao đóng cho phép ẩn thông tin và triển khai chi tiết bên trong một đối tượng, và chỉ tiết lộ cho bên ngoài thông qua các phương thức được định nghĩa sẵn (chỉ có thể truy cập thông qua các phương thức `getter` và `setter`).

![Tính đóng gói (Encapsulation)](https://storage.googleapis.com/algodailyrandomassets/curriculum/blogs/encapsulation.png)

Giúp bảo vệ dữ liệu và hành vi của đối tượng, đảm bảo tính nhất quán và an toàn của ứng dụng.

```java
public class Person {
    // Thuộc tính private
    private String name;

    // Phương thức getter
    public String getName() {
        return name;
    }

    // Phương thức setter
    public void setName(String newName) {
        this.name = newName;
    }
}
```

### 2.2 Tính kế thừa (Inheritance)

Tính kế thừa (Inheritance) cho phép một lớp (class) mới có thể kế thừa các thuộc tính và phương thức từ một lớp khác đã tồn tại (lớp cha).

Lớp mới được gọi là lớp con (`subclass`), và lớp đã tồn tại được gọi là lớp cha (hoặc lớp cơ sở, `superclass`).

![Tính kế thừa (Inheritance)](https://www.scientecheasy.com/wp-content/uploads/2020/07/java-is-a-relationship.png)

```java
// Lớp cha
class Animal {
    void eat() {
        System.out.println("Animal is eating...");
    }
}

// Lớp con kế thừa từ lớp cha
class Dog extends Animal {
    void bark() {
        System.out.println("Dog is barking...");
    }
}

// Sử dụng kế thừa
public class Main {
    public static void main(String[] args) {
        Dog myDog = new Dog();
        myDog.eat(); // Kế thừa phương thức từ lớp cha
        myDog.bark(); // Phương thức riêng của lớp con
    }
}
```

### 2.3 Tính đa hình (Polymorphism)

Tính đa hình là khả năng của đối tượng (object) thể hiện các hành vi khác nhau khi được gọi bằng cùng một phương thức. Cho phép một phương thức có thể thực hiện theo nhiều cách khác nhau.

Tính đa hình giúp làm giảm sự phức tạp của mã lập trình, tăng tính linh hoạt của chương trình.

Tính đa hình được thể hiện thông qua việc sử dụng phương thức ghi đè (override) và nạp chồng (overload):
- **Ghi đè (override)**: Lớp con cung cấp triển khai cho một phương thức đã được định nghĩa trong lớp cha. Sử dụng khi muốn thay đổi hoặc mở rộng cách một phương thức hoạt động trong lớp con.
- **Nạp chồng (overload)**: Nhiều phương thức cùng tên nhưng khác số lượng hoặc kiểu tham số. Sử dụng khi muốn có các phương thức cùng tên nhưng với số lượng hoặc kiểu tham số khác nhau.

```java
// Lớp cha "Hình"
class Shape {
    void draw() {
        System.out.println("Vẽ hình...");
    }
}

// Lớp con "Hình Tròn"
class Circle extends Shape {
    void draw() {
        System.out.println("Vẽ hình tròn...");
    }
}

// Lớp con "Hình Vuông"
class Square extends Shape {
    void draw() {
        System.out.println("Vẽ hình vuông...");
    }
}

public class Main {
    public static void main(String[] args) {
        // Tạo một mảng chứa các hình học
        Shape[] shapes = new Shape[3];
        shapes[0] = new Square();
        shapes[1] = new Circle();
        shapes[2] = new Triangle();

        // Vẽ từng hình trong mảng
        for (Shape shape : shapes) {
            shape.draw();
        }
    }
}
```

### 2.4 Tính trừu tượng (Abstraction)

Tính trừu tượng cho phép chúng ta tạo ra các lớp trừu tượng mà không cần cung cấp các triển khai cụ thể cho các phương thức của chúng.

```java
// Lớp trừu tượng - Hình học
abstract class Shape {
    // Phương thức trừu tượng để tính diện tích
    abstract double calculateArea();
}

// Lớp con - Hình vuông
class Square extends Shape {
    double side;

    Square(double side) {
        this.side = side;
    }

    @Override
    double calculateArea() {
        return side * side;
    }
}

// Lớp con - Hình tròn
class Circle extends Shape {
    double radius;

    Circle(double radius) {
        this.radius = radius;
    }

    @Override
    double calculateArea() {
        return Math.PI * radius * radius;
    }
}

public class Main {
    public static void main(String[] args) {
        Square square = new Square(5);
        Circle circle = new Circle(3);

        System.out.println("Area of square: " + square.calculateArea());
        System.out.println("Area of circle: " + circle.calculateArea());
    }
}
```

## 3. Interface và Abstract class

Interface và Abstract class là hai khái niệm quan trọng trong lập trình hướng đối tượng, được sử dụng để tạo ra các thiết kế linh hoạt và mở rộng.

![Interface và Abstract class](https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEipR6QxqBdNIgDYt6dk9k-FxDvWr-1Jje2qpnd7aWIdwda_QUpJf2rtxFlP3KVIjqoD_jMcyuA4B8IvL50iib1E7MYQQotIwwqfwaEyWkA6ks2U6JFE-rusytxAxrRA_HINc1tRus21Fe0/w1200-h630-p-k-no-nu/interfaces_vs_abstract_Class_Java.png)

### 3.1 Abstract class

Một abstract class giống như một bản thiết kế tổng quát cho các lớp khác. Nó có thể có cả phương thức đã được triển khai (có thân hàm) và phương thức chưa được triển khai (không có thân hàm).

Đặc điểm:
- Một abstract class (lớp trừu tượng) là một lớp không thể được khởi tạo, mà phải được kế thừa bởi các lớp con.
- Một lớp chỉ có thể kế thừa một abstract class (do Java không hỗ trợ đa kế thừa lớp).
- Abstract class có thể có các biến instance, các hàm khởi tạo (constructors), và các phương thức với các mức độ truy cập khác nhau (private, protected, public).

```java
public abstract class Animal {
    abstract void eat();
    
    void sleep() {
        System.out.println("Animal sleeps");
    }
}

public class Dog extends Animal {
    @Override
    public void eat() {
        System.out.println("Dog eats");
    }
}
```

### 3.2 Interface

Một interface là một bản hợp đồng (contract) trong lập trình, định nghĩa một tập hợp các phương thức mà một lớp phải tuân theo. Interface chỉ chứa các phương thức trừu tượng (abstract methods) và các hằng số.

Đặc điểm:
- Interface thường được dùng để định nghĩa các hành vi chung mà nhiều lớp khác nhau có thể thực hiện, giúp tạo ra các thiết kế linh hoạt và dễ mở rộng.
- Các phương thức trong interface đều ngầm định là `public`.
- Các biến trong interface là public, static, và final (hằng số).
- Một lớp có thể implement nhiều interface (đa kế thừa).
- Từ Java 8 trở đi, interface có thể có phương thức mặc định (default methods) và phương thức tĩnh (static methods).

```java
public interface Animal {
    void eat();
    void sleep();
}

public class Dog implements Animal {
    @Override
    public void eat() {
        System.out.println("Dog eats");
    }

    @Override
    public void sleep() {
        System.out.println("Dog sleeps");
    }
}
```

=== THE END ===
