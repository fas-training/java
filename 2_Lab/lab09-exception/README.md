# Exception

Exception là một sự kiện không mong muốn xảy ra trong quá trình thực thi chương trình Java, làm ảnh hưởng đến luồng bình thường của chương trình. Trong Java, exception được chia thành hai loại chính: Checked Exception và Unchecked Exception.

## Checked Exception (Ngoại lệ kiểm tra)

Checked exceptions là các exception mà compiler yêu cầu bạn phải xử lý hoặc khai báo nó trong phần khai báo của phương thức thông qua từ khóa throws. Một số checked exceptions phổ biến:
- `IOException`: Được ném khi có lỗi đọc hoặc ghi file.
- `SQLException`: Được ném khi có lỗi truy vấn cơ sở dữ liệu.
- `ClassNotFoundException`: Được ném khi một class không thể được tìm thấy.

Ví dụ:

```
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Example {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("file.txt"));
        String line = reader.readLine();
        System.out.println(line);
        reader.close();
    }
}
```

## Unchecked Exception (Ngoại lệ không kiểm tra)

Unchecked exceptions là các exception mà compiler không yêu cầu bạn phải xử lý. Thông thường, unchecked exceptions xuất hiện trong quá trình chạy (runtime) và thường là do lỗi logic trong chương trình. Một số unchecked exceptions phổ biến:
- `NullPointerException`: Được ném khi thực hiện một phép toán trên một đối tượng null.
- `ArrayIndexOutOfBoundsException`: Được ném khi truy cập vào một phần tử nằm ngoài biên mảng.
- `ArithmeticException`: Được ném khi thực hiện phép chia cho 0.

Ví dụ:

```
public class Example {
    public static void main(String[] args) {
        String str = null;
        System.out.println(str.length()); // NullPointerException
    }
}
```

## Xử lý Exception trong Java

Xử lý exception có thể được thực hiện bằng cách sử dụng khối try, catch, finally. Một số khái niệm quan trọng liên quan đến xử lý exception:
- `try`: Chứa mã có thể ném exception.
- `catch`: Được sử dụng để xử lý exception, nếu một exception được ném trong khối try.
- `finally`: Chứa mã được thực thi sau cùng, dù có exception hay không. Thường được sử dụng để giải phóng tài nguyên.

Ví dụ:

```
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Example {
    public static void main(String[] args) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("file.txt"));
            String line = reader.readLine();
            System.out.println(line);
        } catch (IOException e) {
            System.err.println("Error reading file: " + e.getMessage());
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                System.err.println("Error closing file: " + e.getMessage());
            }
        }
    }
}
```

## Danh sách Checked và Unchecked Exceptions

https://www.codejava.net/java-core/exception/java-checked-and-unchecked-exceptions
