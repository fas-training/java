package practice;

public class BankAccount {

    private double balance;

    public BankAccount(double initialBalance) {
        this.balance = initialBalance;
    }

    public double getBalance() {
        return balance;
    }

    public void withdraw(double amount) throws IllegalArgumentException, InvalidBalanceException {
        if (amount <= 0) {
            throw new IllegalArgumentException("Withdrawal amount must be greater than zero.");
        }

        if (amount > balance) {
            throw new InvalidBalanceException("Insufficient funds. Current balance: " + balance);
        }

        // Perform the withdrawal
        balance -= amount;
        System.out.println("Withdrawal successful. Remaining balance: " + balance);
    }

}

