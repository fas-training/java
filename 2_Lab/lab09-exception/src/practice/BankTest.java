package practice;

import java.util.InputMismatchException;
import java.util.Scanner;

public class BankTest {

    public static void main(String[] args) throws IllegalArgumentException, InvalidBalanceException {
        Scanner scanner = new Scanner(System.in);

        double withdrawalAmount = 0;
        boolean validInput = false;

        BankAccount bankAccount = new BankAccount(10);

        System.out.println("start");

        while (!validInput) {
            try {
                System.out.print("Enter balance: ");
                withdrawalAmount = scanner.nextDouble();
                validInput = true;

                bankAccount.withdraw(withdrawalAmount);
            } catch (InputMismatchException e) {
                validInput = false;
                System.out.println("Error: " + e.getMessage());
                scanner.nextLine(); // Clear the buffer
            } catch (IllegalArgumentException e) {
                validInput = false;
                System.out.println("Error: " + e.getMessage());
                scanner.nextLine(); // Clear the buffer
            } catch (InvalidBalanceException e) {
                validInput = false;
                System.out.println("Error: " + e.getMessage());
                scanner.nextLine(); // Clear the buffer
            } catch (Exception e) {
                validInput = false;
                scanner.nextLine(); // Clear the buffer
                System.out.println("Invalid input. Please enter a valid number." + e.getMessage());
            }
        }

        scanner.close();

        System.out.println("end");
    }

}
