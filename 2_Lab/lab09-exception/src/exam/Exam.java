package exam;

public class Exam {

    // Hàm ném exception IllegalArgumentException
    private static void divideByZero() throws MyCheckedException {
        int dividend = 10;
        int divisor = 0;

        if (divisor == 0) {
            throw new MyCheckedException("Cannot divide by zero");
        }

//        int result = dividend / divisor;
//        System.out.println("Result: " + result);
    }

    // Hàm gọi hàm divideByZero và chuyển tiếp exception
    private static void performDivision() throws MyCheckedException {
        try {
            divideByZero();
        } catch (MyCheckedException e) {
            System.err.println("Caught exception in performDivision: " + e.getMessage());
            throw e; // Chuyển tiếp exception lên trên cùng
        }
    }

    // Hàm gọi hàm divideByZero
    public static void main(String[] args) {
        try {
            performDivision();
        } catch (MyCheckedException e) {
            System.err.println("Caught exception in main: " + e.getMessage());
        }
    }

}
