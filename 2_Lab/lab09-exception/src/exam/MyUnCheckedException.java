package exam;

// Checked exception tự định nghĩa

// Nếu bạn kế thừa từ RuntimeException, thì đó là một unchecked exception và không có yêu cầu xử lý hoặc khai báo.
class MyUnCheckedException extends RuntimeException {
    public MyUnCheckedException() {
        super();
    }

    public MyUnCheckedException(String message) {
        super(message);
    }
}
