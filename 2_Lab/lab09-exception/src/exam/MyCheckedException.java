package exam;

// Checked exception tự định nghĩa

//Nếu bạn kế thừa từ Exception, thì đó là một checked exception và bạn sẽ buộc phải xử lý nó hoặc
// khai báo nó trong khai báo của phương thức sử dụng throws.
class MyCheckedException extends Exception {
    public MyCheckedException() {
        super();
    }

    public MyCheckedException(String message) {
        super(message);
    }
}
