package exam;

public class XuLyChuoi {

    public static String kiemTraChuoi(String chuoi) throws LoiNhapLieuRong, LoiNhapLieu {
        // kiem tar null
        if (chuoi == null) {
            throw new LoiNhapLieuRong("loi nhap gia tri null");
        }

        // kiem tra do dai
        if (chuoi.trim().length() != 6) {
            throw new LoiNhapLieu("yeu cau nhap 6 ki tu");
        }

        // kiem tra chuoi bat dau FA..

        if (chuoi.startsWith("fa")) {
            ////
            throw new LoiNhapLieu("nhap thieu gia tri FA");
        }

        return chuoi + " hop le";
    }

}
