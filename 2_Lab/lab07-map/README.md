# Map

HashMap và TreeMap là hai lớp trong Java thực hiện giao diện Map và được sử dụng để lưu trữ dữ liệu dưới dạng cặp key-value. Dưới đây là một so sánh giữa HashMap và TreeMap:

## So sánh HashMap và TreeMap

Dưới đây là một số so sánh giữa HashMap và TreeMap:

**Thứ tự của các phần tử**:
- HashMap: Không đảm bảo thứ tự của các phần tử, tức là không giữ thứ tự chèn hoặc bất kỳ thứ tự nào khác.
- TreeMap: Sắp xếp các phần tử theo thứ tự tăng dần của key. Bảo đảm rằng bạn sẽ nhận được các phần tử theo thứ tự key.

**Hiệu suất**:
- HashMap: Thao tác truy cập, chèn và xóa có thể được thực hiện trong thời gian O(1) trong trường hợp trung bình. Nó có hiệu suất tốt khi không cần thứ tự cụ thể của các phần tử.
- TreeMap: Thao tác truy cập, chèn và xóa có độ phức tạp là O(log n). Nó hiệu quả khi cần duy trì thứ tự của các phần tử.

**Null key**:
- HashMap: Chấp nhận một key là null.
- TreeMap: Không chấp nhận key là null vì nó sử dụng thứ tự tự nhiên của key để lưu trữ phần tử.

**Tính năng đặc biệt**:
- HashMap: Nhanh chóng và linh hoạt, thích hợp cho nhiều tình huống. Thích hợp khi bạn cần thao tác nhanh trên dữ liệu mà không quan trọng về thứ tự.
- TreeMap: Cung cấp thứ tự tự nhiên của key, phù hợp khi bạn cần giữ thứ tự tăng dần của key.

## Ví dụ

Dưới đây là một ví dụ đơn giản để minh họa cách sử dụng HashMap và TreeMap:

```
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Example {
    public static void main(String[] args) {
        // Sử dụng HashMap để lưu thông tin số điểm sinh viên
        Map<String, Integer> hashMap = new HashMap<>();
        hashMap.put("John", 85);
        hashMap.put("Alice", 92);
        hashMap.put("Bob", 78);
        hashMap.put("Eva", 90);

        // Hiển thị thông tin số điểm sinh viên từ HashMap
        System.out.println("HashMap:");
        for (Map.Entry<String, Integer> entry : hashMap.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }

        // Sử dụng TreeMap với Comparator.reverseOrder() để sắp xếp giảm dần
        Map<String, Integer> treeMap = new TreeMap<>(java.util.Collections.reverseOrder());
        treeMap.putAll(hashMap);

        // Hiển thị thông tin số điểm sinh viên từ TreeMap (giảm dần)
        System.out.println("\nTreeMap (descending order):");
        for (Map.Entry<String, Integer> entry : treeMap.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }
}
```
