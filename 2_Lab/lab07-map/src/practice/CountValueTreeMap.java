package practice;

import java.util.Map;
import java.util.TreeMap;

public class CountValueTreeMap {

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 2, 4, 1, 2, 5, 4, 2, 3, 5, 2, 1, 4};

        // Tạo một HashMap để đếm số lần xuất hiện của từng phần tử
        TreeMap<Integer, Integer> treeMap = new TreeMap<>();
//        treeMap.put(null, 0);


        // Đếm số lần xuất hiện của từng phần tử trong mảng
        for (int element : array) {
            /*
            Hàm getOrDefault() được sử dụng để lấy giá trị tương ứng với một key từ một Map. Nếu key tồn tại,
            nó sẽ trả về giá trị của key đó; nếu key không tồn tại, nó sẽ trả về một giá trị mặc định được chỉ định trước đó.
             */
//            treeMap.put(element, treeMap.getOrDefault(element, 0) + 1);

            if (treeMap.containsKey(element)) {
                int giaTriCu = treeMap.get(element);
                treeMap.put(element, giaTriCu + 1);
            } else {
                treeMap.put(element, 1);
            }
        }

//        // Hiển thị kết quả
//        for (Map.Entry<Integer, Integer> entry : treeMap.entrySet()) {
//            System.out.println("Số " + entry.getKey() + " xuất hiện " + entry.getValue() + " lần");
//        }
    }

}
