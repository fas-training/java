package practice;

import java.util.HashMap;
import java.util.Map;

public class CountValueHashMap {

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 2, 4, 1, 2, 5, 4, 2, 3, 5, 2, 1, 4};

        // Tạo một HashMap để đếm số lần xuất hiện của từng phần tử
        HashMap<Integer, Integer> hashMap = new HashMap<>();

        // Đếm số lần xuất hiện của từng phần tử trong mảng
        for (int element : array) {

            System.out.println(element);

            /*
            Hàm getOrDefault() được sử dụng để lấy giá trị tương ứng với một key từ một Map. Nếu key tồn tại,
            nó sẽ trả về giá trị của key đó; nếu key không tồn tại, nó sẽ trả về một giá trị mặc định được chỉ định trước đó.
             */
            hashMap.put(element, hashMap.getOrDefault(element, 0) + 1);

            if (hashMap.containsKey(element)) {
                hashMap.put(element, hashMap.get(element) + 1);
            } else {
                hashMap.put(element, 1);
            }


        }

        // Hiển thị kết quả
        for (Map.Entry<Integer, Integer> entry : hashMap.entrySet()) {
            System.out.println("Số " + entry.getKey() + " xuất hiện " + entry.getValue() + " lần");
        }
    }

}
