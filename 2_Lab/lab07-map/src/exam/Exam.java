package exam;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Exam {

    public static void getHashMap() {
        // Sử dụng HashMap
        Map<String, Integer> hashMap = new HashMap<>();

        // Thêm các phần tử vào HashMap
        hashMap.put("three", 3);
        hashMap.put("one", 1);
        hashMap.put("four", 4);
        hashMap.put("two", 2);

        // Lặp qua các phần tử của HashMap
        for (Map.Entry<String, Integer> entry : hashMap.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }

    public static void getTreeMap() {
        // Sử dụng TreeMap với Comparator để sắp xếp giảm dần
        Map<String, Integer> treeMap = new TreeMap<>(Comparator.reverseOrder());

        // Thêm các phần tử vào TreeMap
        treeMap.put("three", 3);
        treeMap.put("one", 1);
        treeMap.put("four", 4);
        treeMap.put("two", 2);

        // Lặp qua các phần tử của TreeMap
        for (Map.Entry<String, Integer> entry : treeMap.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
    }

    public static void main(String[] args) {
        getHashMap();

        getTreeMap();
    }

}
