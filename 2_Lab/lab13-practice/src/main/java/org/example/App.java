package org.example;

import org.example.model.*;
import org.example.service.TaskManager;
import org.example.util.FormatUtil;

import java.util.Map;

public class App
{
    public static void main( String[] args )
    {
        // Khởi tạo phòng ban
        Department itDepartment = new Department("001", "IT", "Information Technology");
        Department hrDepartment = new Department("002", "HR", "Human Resources");
        Department financeDepartment = new Department("003", "Finance", "Finance and Accounting");

        // Khởi tạo nhân viên
        Manager m1 = new Manager("M001", "Het Truong", 5, 5000, hrDepartment, 1000);
        Staff s1 = new Staff("S001", "Huy Vu", 3, 3000, financeDepartment, "HR Assistant");
        Worker w1 = new Worker("W001", "Phuc Pham", 2, 2000, itDepartment, "Night Shift");
        Worker w2 = new Worker("W002", "Duong Truong", 5, 3000, itDepartment, "Day Shift");
        Worker w3 = new Worker("W003", "Minh Nguyen", 3, 2500, itDepartment, "Day Shift");

        w2.displayInfo();

        // Tính lương cơ bản nhân viên
        double salaryBasicW3 =  w3.calculateBasicSalary();
        System.out.println("W3: " + salaryBasicW3);

        // Tính lương nhân viên với phần cộng thêm
        double salaryBonusW3 =  w3.calculateBonusSalary(500);
        System.out.println("W3: " + FormatUtil.toFormatNumber(salaryBonusW3));

        // Khởi tạo công
        Task task1 = new Task("T001", "Fix bug in the software", w1, 8);
        Task task2 = new Task("T002", "Implement new feature", w1, 10);
        Task task3 = new Task("T003", "Perform routine maintenance", w2, 6);
        Task task4 = new Task("T004", "Update employee records", m1, 2);
        Task task5 = new Task("T005", "Generate financial report", w3, 5);

        TaskManager taskManager = new TaskManager();
        taskManager.addTask(task1);
        taskManager.addTask(task2);
        taskManager.addTask(task3);
        taskManager.addTask(task4);
        taskManager.addTask(task5);

        // Tính tổng số giờ làm việc của từng nhân viên
        taskManager.displayTotalHoursWorked(w3);

        Map<String, Double> totalHoursPerEmployee = taskManager.calculateTotalHoursWorkedPerEmployee();
        for (Map.Entry<String, Double> entry : totalHoursPerEmployee.entrySet()) {
            System.out.println("Employee ID: " + entry.getKey() + ", Total Hours Worked: " + entry.getValue());
        }
    }

}
