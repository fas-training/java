package org.example.util;

import java.text.DecimalFormat;

public class FormatUtil {

    public static String toFormatNumber(double number) {
        DecimalFormat formatter = new DecimalFormat("#,###.0");
        String formattedNumber = formatter.format(number);
        return formattedNumber;
    }

}
