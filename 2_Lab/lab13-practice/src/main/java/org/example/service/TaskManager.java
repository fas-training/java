package org.example.service;

import org.example.model.Employee;
import org.example.model.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskManager {

    private static List<Task> tasks;

    public TaskManager() {
        this.tasks = new ArrayList<>();
    }

    // Trả về danh sách tất cả các công việc.
    public List<Task> getAllTasks() {
        return tasks;
    }

    // Tìm kiếm công việc theo taskId và trả về nếu nó tồn tại.
    public Task getTaskById(String taskId) {
        for (Task task : tasks) {
            if (task.getTaskId().equals(taskId)) {
                return task;
            }
        }
        return null; // Return null if task not found
    }

    // Thêm một công việc vào danh sách công việc.
    public void addTask(Task task) {
        tasks.add(task);
    }

    // Cập nhật thông tin của một công việc theo taskId.
    public void updateTask(Task updatedTask) {
        for (int i = 0; i < tasks.size(); i++) {
            if (tasks.get(i).getTaskId().equals(updatedTask.getTaskId())) {
                tasks.set(i, updatedTask);
                return;
            }
        }
    }

    // Xóa một công việc dựa trên taskId.
    public void deleteTask(String taskId) {
        tasks.removeIf(task -> task.getTaskId().equals(taskId));
    }

    // Phương thức tính tổng số giờ làm việc của từng nhân viên
    public double displayTotalHoursWorked(Employee employee) {
        double totalHoursWorked = 0.0;
        // Assuming we have a list of tasks assigned to each employee
        // Here, we iterate over the tasks and calculate the total hours worked
        for (Task task : getAllTasks()) {
            totalHoursWorked += task.getHoursOfWorked();
        }

        return totalHoursWorked;
    }

    // Phương thức tính tổng số giờ làm việc của từng nhân viên
    public Map<String, Double> calculateTotalHoursWorkedPerEmployee() {
        Map<String, Double> totalHoursPerEmployee = new HashMap<>();

        for (Task task : tasks) {
            String empId = task.getAssignedTo().getEmpId();
            double hoursWorked = task.getHoursOfWorked();

            // Update total hours for the employee
            totalHoursPerEmployee.put(empId, totalHoursPerEmployee.getOrDefault(empId, 0.0) + hoursWorked);
        }

        return totalHoursPerEmployee;
    }

}
