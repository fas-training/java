package org.example.model;

public abstract class Employee {

    private String empId; // mã nhân viên
    private String fullName; // họ tên
    private int yearsOfService; // tổng số năm làm việc
    private double salary; // lương cơ bản
    private Department department;  // bộ phận


    public Employee() {
    }

    public Employee(String empId, String fullName, int yearsOfService, double salary, Department department) {
        this.empId = empId;
        this.fullName = fullName;
        this.yearsOfService = yearsOfService;
        this.salary = salary;
        this.department = department;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getYearsOfService() {
        return yearsOfService;
    }

    public void setYearsOfService(int yearsOfService) {
        this.yearsOfService = yearsOfService;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    // hiển thị thông tin cơ bản nhân viên
    public void displayInfo() {
        System.out.println("Employee ID: " + empId);
        System.out.println("Full Name: " + fullName);
        System.out.println("Years of Service: " + yearsOfService);
        System.out.println("Salary: " + salary);
        System.out.println("Department: " + department.getName());
        displayAdditionalInfo();
    }

    // Phương thức này sẽ được thực hiện bởi các lớp con
    public abstract void displayAdditionalInfo();

    // Tính lương cơ bản nhân viên
    public double calculateBasicSalary() {
        return this.getSalary() * 0.95;
    }

    // Tính lương nhân viên với phần cộng thêm
    public double calculateBonusSalary(double bonus) {
        return this.calculateBasicSalary() + bonus;
    }

}
