package org.example.model;

public class Staff extends Employee {

    private String position; // vị trí làm việc

    public Staff() {
    }

    public Staff(String empId, String fullName, int yearsOfService, double salary, Department department, String position) {
        super(empId, fullName, yearsOfService, salary, department);
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public void displayAdditionalInfo() {
        System.out.println("Position: " + position);
    }

}
