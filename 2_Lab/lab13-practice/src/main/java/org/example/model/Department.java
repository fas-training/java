package org.example.model;

public class Department {

    private String depId;
    private String name;
    private String description;

    public Department() {
    }

    public Department(String depId, String name, String description) {
        this.depId = depId;
        this.name = name;
        this.description = description;
    }

    public String getDepId() {
        return depId;
    }

    public void setDepId(String depId) {
        this.depId = depId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
