package org.example.model;

public class Task {

    private String taskId; // mã công việc
    private String description; // mô tả công việc
    private Employee assignedTo; // nhân viên được giao công việc
    private double hoursOfWorked; // số giờ làm việc

    public Task() {
    }

    public Task(String taskId, String description, Employee assignedTo, double hoursOfWorked) {
        this.taskId = taskId;
        this.description = description;
        this.assignedTo = assignedTo;
        this.hoursOfWorked = hoursOfWorked;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Employee getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(Employee assignedTo) {
        this.assignedTo = assignedTo;
    }

    public double getHoursOfWorked() {
        return hoursOfWorked;
    }

    public void setHoursOfWorked(double hoursOfWorked) {
        this.hoursOfWorked = hoursOfWorked;
    }

}
