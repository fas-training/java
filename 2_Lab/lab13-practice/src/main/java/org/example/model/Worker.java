package org.example.model;

public class Worker extends Employee {

    private String shift; // loại ca (ca ngày / ca đêm)

    public Worker() {
    }

    public Worker(String empId, String fullName, int yearsOfService, double salary, Department department, String shift) {
        super(empId, fullName, yearsOfService, salary, department);
        this.shift = shift;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    @Override
    public void displayAdditionalInfo() {
        System.out.println("Shift: " + shift);
    }

}
