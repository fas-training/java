package org.example.model;

public class Manager extends Employee {

    private double allowance; // phụ cấp

    public Manager() {
    }

    public Manager(String empId, String fullName, int yearsOfService, double salary, Department department, double allowance) {
        super(empId, fullName, yearsOfService, salary, department);
        this.allowance = allowance;
    }

    public double getAllowance() {
        return allowance;
    }

    public void setAllowance(double allowance) {
        this.allowance = allowance;
    }

    @Override
    public void displayAdditionalInfo() {
        System.out.println("Allowance: " + allowance);
    }

    @Override
    public double calculateBasicSalary() {
        return super.calculateBasicSalary() + allowance;
    }

    @Override
    public double calculateBonusSalary(double bonus) {
        return super.getSalary() + bonus + allowance;
    }

}
