package org.example;

import org.example.model.Department;
import org.example.model.Manager;
import org.example.model.Staff;
import org.example.model.Worker;
import org.junit.Assert;
import org.junit.Test;

public class SalaryTest {

    @Test
    public void testCalculateBasicSalary() {
        // Khởi tạo phòng ban
        Department itDepartment = new Department("001", "IT", "Information Technology");
        Department hrDepartment = new Department("002", "HR", "Human Resources");
        Department financeDepartment = new Department("003", "Finance", "Finance and Accounting");

        // Khởi tạo nhân viên
        Manager m1 = new Manager("M001", "Het Truong", 5, 5000, hrDepartment, 1000);
        Staff s1 = new Staff("S001", "Huy Vu", 3, 3000, financeDepartment, "HR Assistant");
        Worker w1 = new Worker("W001", "Phuc Pham", 2, 2000, itDepartment, "Night Shift");

        // Act
        double basicSalaryA = m1.calculateBasicSalary();
        double basicSalaryB = s1.calculateBasicSalary();
        double basicSalaryC = w1.calculateBasicSalary();

        // Assert
        Assert.assertEquals(5750.0, basicSalaryA, 0.001); // delta is 0.001, delta là giá trị cho phép sai số nhỏ trong so sánh.
        Assert.assertEquals(2850.0, basicSalaryB, 0.001);
        Assert.assertEquals(1900.0, basicSalaryC, 0.001);
    }

    @Test
    public void testCalculateBonusSalary() {
        // Arrange
        Department department = new Department("001", "IT", "Information Technology");
        Manager manager = new Manager("M001", "John Doe", 5, 5000, department, 1000);

        // Act
        double bonusSalaryA = manager.calculateBonusSalary(0);
        double bonusSalaryB = manager.calculateBonusSalary(100);
        double bonusSalaryC = manager.calculateBonusSalary(500);

        // Assert
        Assert.assertEquals(6000.0, bonusSalaryA, 0.001);
        Assert.assertEquals(6100.0, bonusSalaryB, 0.001);
        Assert.assertEquals(6500.0, bonusSalaryC, 0.001);
    }

}
