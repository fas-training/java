package org.example;

import org.example.model.Department;
import org.example.model.Manager;
import org.example.model.Staff;
import org.example.model.Worker;
import org.junit.Assert;
import org.junit.Test;

public class EmployeeTest {

    @Test
    public void testManager() {
        // Khởi tạo phòng ban
        Department itDepartment = new Department("001", "IT", "Information Technology");

        // Khởi tạo nhân viên
        Manager m1 = new Manager("M001", "Het Truong", 5, 5000, itDepartment, 1000);

        // Act
        String empId = m1.getEmpId();

        // Assert
        Assert.assertEquals("M001", empId);
    }

    @Test
    public void testStaff() {
        // Khởi tạo phòng ban
        Department hrDepartment = new Department("002", "HR", "Human Resources");

        // Khởi tạo nhân viên
        Staff s1 = new Staff("S001", "Huy Vu", 3, 3000, hrDepartment, "HR Assistant");

        // Act
        String empId = s1.getEmpId();

        // Assert
        Assert.assertEquals("S001", empId);
    }

    @Test
    public void testWorker() {
        // Khởi tạo phòng ban
        Department hrDepartment = new Department("002", "HR", "Human Resources");

        // Khởi tạo nhân viên
        Worker w1 = new Worker("W003", "Minh Nguyen", 3, 2500, hrDepartment, "Day Shift");

        // Act
        String empId = w1.getEmpId();

        // Assert
        Assert.assertEquals("W003", empId);
    }

}
