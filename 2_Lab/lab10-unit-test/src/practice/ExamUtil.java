package practice;

public class ExamUtil {

    public String sayHello(String lang) {
        if (lang == null) {
            return null;
        }

        if (lang.equalsIgnoreCase("VI")) {
            return "Xin chào";
        } else {
            if (lang.equalsIgnoreCase("EN")) {
                return "Hello";
            } else {
                return "Chao Xìn";
            }
        }
    }

}
