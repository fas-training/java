package practice;

public class CalculatorUtil {

    public int sum(int x, int y) {
        return x + y;
    }

    public int divide(int dividend, int divisor) {
        if (divisor == 0) {
            throw new IllegalArgumentException("Cannot divide by zero");
        }

        return dividend / divisor;
    }

}
