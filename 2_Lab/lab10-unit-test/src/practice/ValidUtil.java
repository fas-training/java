package practice;

public class ValidUtil {

    public static boolean isValidEmail(String email) {
        String emailRegex = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$";
        return email.matches(emailRegex);
    }

    public static boolean isValidFormat(String input) {
        String formatRegex = "[A-Z]{3}-\\d{5}"; // ABC-12345
         return input.matches(formatRegex);
    }

}
