package test;

import static org.junit.Assert.*;
import org.junit.Test;
import practice.ValidUtil;

public class EmailTest {

    @Test
    public void testIsValidEmailValid() {
        assertTrue(ValidUtil.isValidEmail("test@example.com"));
        assertTrue(ValidUtil.isValidEmail("john.doe123@domain.com"));
        assertTrue(ValidUtil.isValidEmail("user.email@example-domain.com"));
    }

    @Test
    public void testIsValidEmailInvalid() {
        assertFalse(ValidUtil.isValidEmail("invalid-email")); // Thiếu ký tự '@'
        assertFalse(ValidUtil.isValidEmail("user@")); // Thiếu domain
        assertFalse(ValidUtil.isValidEmail("@domain")); // Thiếu user
    }
    
}
