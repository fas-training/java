package test;

import org.junit.Assert;
import org.junit.Test;
import practice.ValidUtil;

public class FormatTest {

    @Test
    public void testIsValidFormatValid() {
        Assert.assertTrue(ValidUtil.isValidFormat("ABC-12345"));
        Assert.assertTrue(ValidUtil.isValidFormat("XYZ-67890"));
    }

    @Test
    public void testIsValidFormatInvalid() {
        Assert.assertFalse(ValidUtil.isValidFormat("ABC-1234")); // Số lượng chữ số không đúng
        Assert.assertFalse(ValidUtil.isValidFormat("XYZ-ABCDE")); // Không phải là số
        Assert.assertFalse(ValidUtil.isValidFormat("abc-12345")); // Chữ cái không đúng chữ hoa
        Assert.assertFalse(ValidUtil.isValidFormat("ABC_12345")); // Ký tự đặc biệt không hợp lệ
    }

}
