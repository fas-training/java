package test;

import org.junit.*;
import practice.CalculatorUtil;
import practice.ExamUtil;

public class ExamTest {

    private CalculatorUtil calculator;

    @BeforeClass
    public static void setUpClass() {
        System.out.println("Before Class - Initialize resources once before any test method");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("After Class - Clean up resources once after all test methods");
    }

    @Before
    public void setUp() {
        calculator = new CalculatorUtil();
        System.out.println("Before - Initialize resources before each test method");
    }

    @After
    public void tearDown() {
        calculator = null;
        System.out.println("After - Clean up resources after each test method");
    }

    @Test
    public void testAdd1() {
        int result = calculator.sum(2, 3);
        Assert.assertEquals("Error SUM method", 5, result);
    }

    @Test
    public void testAdd2() {
        int result = calculator.sum(2, 3);
        Assert.assertNotEquals("Error SUM method", 4, result);
    }

    @Test
    public void testSayHello() {
        ExamUtil examUtil = new ExamUtil();
        String result = examUtil.sayHello(null);
        Assert.assertEquals("Error SUM method", null, result);
    }

    @Test
    public void testDivideByNonZero() {
        // Arrange
        int dividend = 10;
        int divisor = 2;

        // Act
        int result = calculator.divide(dividend, divisor);

        // Assert
        int expectedResult = 5;
        Assert.assertEquals("Dividing by non-zero should return the quotient", expectedResult, result);
    }

    @Test
    public void testDivideByZero() {
        // Arrange
        int dividend = 10;
        int divisor = 0;

        // Act & Assert
        NullPointerException exception = Assert.assertThrows(
                NullPointerException.class,
                () -> calculator.divide(dividend, divisor)
        );

        // Assert the exception message
        String expectedMessage = "Cannot divide by zero";
        String actualMessage = exception.getMessage();
        Assert.assertTrue(actualMessage.contains(expectedMessage));
    }

}
