package exam;

import org.junit.*;

public class StudentTest {

    Student student = null;

    @Before
    public void initData() {
        student = new Student();
        System.out.println("BeforeClass:");

    }

    @After
    public void clearData() {
        System.out.println("AfterClass:");
    }


    @Test
    public void testA() {
        student = new Student();
        student.setName("A");

        Assert.assertEquals("A", student.getName());

        System.out.println("testA");
    }

    @Test
    public void testB() {
        student = new Student();
        student.setName("B");

        Assert.assertEquals("B", student.getName());
        System.out.println("testB");
    }

}
