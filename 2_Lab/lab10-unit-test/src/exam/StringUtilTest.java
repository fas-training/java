package exam;

import org.junit.Assert;
import org.junit.Test;

public class StringUtilTest {

    @Test
    public void testVietNam() {
        StringUtil stringUtil = new StringUtil();

        String result = stringUtil.sayHello("vi");

        Assert.assertFalse(1 > 1);

        Assert.assertEquals("xin chao", result);
    }

//    @Test
//    public void testEn() {
//        StringUtil stringUtil = new StringUtil();
//
//        String result = stringUtil.sayHello("en");
//
//        Assert.assertEquals("loi", "hello", result);
//    }
//
//    @Test
//    public void testDefault() {
//        StringUtil stringUtil = new StringUtil();
//
//        String result = stringUtil.sayHello("");
//
//        Assert.assertEquals("loi", "chao xin", result);
//    }

}
