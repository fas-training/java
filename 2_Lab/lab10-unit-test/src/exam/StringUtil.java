package exam;

public class StringUtil {

    public String sayHello(String lang) {
        if (lang == null) {
            throw new NullPointerException("chuoi null");
        }

        switch (lang) {
            case "vi":
                return "xin chao";
            case "en":
                return "hellos";
            default:
                return "chao xin";
        }
    }

}
