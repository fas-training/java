# Unit Test

Unit testing là một phần quan trọng trong quá trình phát triển phần mềm, giúp đảm bảo rằng các thành phần cụ thể của chương trình hoạt động đúng như dự kiến. Dưới đây là một số khái niệm và cách thực hiện unit testing trong Java với JUnit, một trong những framework kiểm thử phổ biến nhất.

## Khái Niệm Cơ Bản

- `Unit Test`: Là việc kiểm thử một đơn vị nhỏ nhất của mã nguồn, thường là một phương thức, một lớp nhỏ hoặc một module nhỏ.
- `Test Case`: Một bộ dữ liệu đầu vào và kết quả mong đợi được sử dụng để kiểm thử một đơn vị cụ thể của mã nguồn.
- `JUnit`: Là một framework kiểm thử cho Java. 

# Ví dụ

Giả sử bạn có một lớp Calculator với một phương thức add:

```
public class Calculator {
    public int add(int a, int b) {
        return a + b;
    }
}
```

Viết unit test cho phương thức này:

```
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculatorTest {

    private Calculator calculator;

    @BeforeEach
    public void setUp() {
        calculator = new Calculator();
    }

    @Test
    public void testAddPositiveNumbers() {
        int result = calculator.add(2, 3);
        Assertions.assertEquals(5, result, "Adding positive numbers should return the sum");
    }

    @Test
    public void testAddNegativeNumbers() {
        int result = calculator.add(-2, -3);
        Assertions.assertEquals(-5, result, "Adding negative numbers should return the sum");
    }

    @Test
    public void testAddZero() {
        int result = calculator.add(0, 0);
        Assertions.assertEquals(0, result, "Adding zero to zero should return zero");
    }
}
```

Trong ví dụ trên:
- @Test đánh dấu các phương thức là test case.
- @BeforeEach đánh dấu phương thức setUp, chạy trước mỗi test case, để thiết lập trạng thái ban đầu (tạo một đối tượng Calculator).
- Assertions cung cấp các phương thức để kiểm tra kết quả của test case. Trong ví dụ, chúng ta sử dụng assertEquals để so sánh kết quả của phương thức add với giá trị mong đợi.
