package org.fas.model;

public class Student implements Comparable<Student> {

    private int id;
    private String name;
    private int age;

    public Student() {
    }

    public Student(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    // Ghi đè phương thức toString() để biểu diễn thông tin mô tả của đối tượng Student dễ đọc hơn
    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    // Ghi đè phương thức compareTo từ Comparable interface để so sánh theo tên
    @Override
    public int compareTo(Student o) {
        // So sánh theo tên
        int compareByName = this.name.compareTo(o.name);

        // Nếu tên bằng nhau, thì so sánh theo tuổi
        if (compareByName == 0) {
            return o.age - this.age;
        }

        return compareByName;
    }

}
