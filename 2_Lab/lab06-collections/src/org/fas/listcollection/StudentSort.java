package org.fas.listcollection;

import org.fas.model.Student;

import java.util.Comparator;

public class StudentSort implements Comparator<Student> {

    @Override
    public int compare(Student o1, Student o2) {
        // So sánh theo tên
        int compareByName = o1.getName().compareTo(o2.getName());

        // Nếu tên bằng nhau, thì so sánh theo tuổi
        if (compareByName == 0) {
            return o2.getAge() - o1.getAge();
        }

        return compareByName;
    }

}
