package org.fas.listcollection;

import org.fas.model.Student;

import java.util.ArrayList;
import java.util.Collections;

public class ArrayListCollection {

    // Khai báo một ArrayList để lưu trữ các đối tượng Student
    private ArrayList<Student> studentList = new ArrayList<>();

    // Phương thức để thêm một học sinh mới vào danh sách
    public void addStudent(int id, String name, int age) {
        // Tạo một đối tượng Student mới
        Student newStudent = new Student(id, name, age);

        // Thêm học sinh mới vào ArrayList
        studentList.add(newStudent);
    }

    // Phương thức để thêm một học sinh mới vào danh sách
    public void addStudent(Student st) {
        // Thêm học sinh mới vào ArrayList
        if (st != null) {
            studentList.add(st);
        }
    }

    // Phương thức để cập nhật thông tin học sinh dựa trên một đối tượng Student mới
    public boolean updateStudent(Student st) {
        // Duyệt qua danh sách học sinh
        for (int i = 0; i < studentList.size(); i++) {
            // Kiểm tra nếu id của học sinh khớp với id của đối tượng Student cần cập nhật
            if (studentList.get(i).getId() == st.getId()) {
                // Cập nhật thông tin học sinh tại vị trí tìm thấy
                studentList.set(i, st);
                return true; // Trả về true nếu cập nhật thành công
            }
        }
        return false; // Trả về false nếu không tìm thấy học sinh với id đã cho
    }

    // Phương thức để cập nhật Name dựa theo Id
    public boolean updateStudentByName(int id, String name) {
        // Duyệt qua danh sách học sinh
        for (Student student : studentList) {
            // Kiểm tra nếu tên học sinh khớp với tên cần cập nhật
            if (student.getId() == id) {
                // Cập nhật thông tin học sinh
                student.setName(name);
                return true; // Trả về true nếu cập nhật thành công
            }
        }
        return false; // Trả về false nếu không tìm thấy học sinh với tên đã cho
    }

    // Phương thức để xóa học sinh dựa trên id
    public boolean deleteStudent(Student st) {
        // Duyệt qua danh sách học sinh
        for (int i = 0; i < studentList.size(); i++) {
            // Kiểm tra nếu id của học sinh khớp với id cần xóa
            if (studentList.get(i).getId() == st.getId()) {
                // Xóa học sinh tại vị trí tìm thấy
                studentList.remove(i);
                return true; // Trả về true nếu xóa thành công
            }
        }
        return false; // Trả về false nếu không tìm thấy học sinh với id đã cho
    }

    // Phương thức để xóa học sinh dựa trên id sử dụng removeIf
    public boolean deleteStudentById(int id) {
        // Sử dụng removeIf để xóa học sinh với id khớp
        return studentList.removeIf(student -> student.getId() == id);
    }

    // Phương thức để in ra tất cả học sinh trong danh sách
    public void printAllStudents(ArrayList<Student> students) {
        // Duyệt qua danh sách học sinh và in ra từng học sinh
        for (Student student : students) {
            System.out.println(student);
        }
    }

    // Phương thức main để kiểm tra
    public static void main(String[] args) {
        // Tạo một instance của ArrayListCollection
        ArrayListCollection collection = new ArrayListCollection();

        // Tạo một số đối tượng Student
        Student student1 = new Student(1, "Duong Truong", 20);
        Student student2 = new Student(2, "Huy Vu", 22);
        Student student3 = new Student(3, "Phuong Nguyen", 18);
        Student student4 = new Student(4, "Huy Vu", 18);
        Student student5 = new Student(5, "Ngoc Pham", 30);

        // Thêm các học sinh vào collection
        collection.addStudent(student1);
        collection.addStudent(student2);
        collection.addStudent(student3);
        collection.addStudent(student4);
        collection.addStudent(student5);

        // Xóa học sinh theo id
        boolean deleted = collection.deleteStudentById(1);

        // In kết quả xóa
        if (deleted) {
            System.out.println("Xóa dữ liệu thành công.");
        } else {
            System.out.println("Không tìm thấy học sinh với ID đã cho.");
        }

        // In danh sách học sinh
//        for (Student student : collection.studentList) {
//            System.out.println(student);
//        }

        ArrayList<Student> studentList = collection.studentList;
        Collections.sort(studentList);

//        // In danh sách học sinh sau khi đã sắp xếp
//        collection.printAllStudents(studentList);

        ArrayList<Student> studentList2 = collection.studentList;
        Collections.sort(studentList2, new StudentSort());
        // In danh sách học sinh sau khi đã sắp xếp
        collection.printAllStudents(studentList2);
    }

}
