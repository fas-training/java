package org.fas.listcollection;

import org.fas.model.Student;

import java.util.Comparator;

// Comparator để so sánh Student theo tuổi
public class StudentSortByAge implements Comparator<Student> {

    @Override
    public int compare(Student o1, Student o2) {
        return Integer.compare(o1.getAge(), o2.getAge());
    }

}
