package org.fas.listcollection;

import org.fas.model.Student;

import java.util.Collections;
import java.util.LinkedList;

public class LinkedListCollection {

    private LinkedList<Student> studentList;

    public LinkedListCollection() {
        this.studentList = new LinkedList<>();
    }

    // Thêm mới một Student vào danh sách
    public void addStudent(Student student) {
        studentList.add(student);
    }

    // Đọc và in ra tất cả các Student trong danh sách
    public void readAll() {
        for (Student student : studentList) {
            System.out.println(student);
        }
    }

    // Cập nhật thông tin của một Student dựa trên id
    public boolean update(Student updatedStudent) {
        for (Student student : studentList) {
            if (student.getId() == updatedStudent.getId()) {
                // Update thông tin của student tại vị trí tương ứng
                student.setName(updatedStudent.getName());
                student.setAge(updatedStudent.getAge());
                return true; // Trả về true nếu cập nhật thành công
            }
        }
        return false; // Trả về false nếu không tìm thấy Student để cập nhật
    }

    // Xóa một Student khỏi danh sách dựa trên id
    public boolean delete(int studentId) {
        // Duyệt qua danh sách để tìm và xóa Student có id phù hợp
        for (Student student : studentList) {
            if (student.getId() == studentId) {
                studentList.remove(student);
                return true; // Trả về true nếu xóa thành công
            }
        }
        return false; // Trả về false nếu không tìm thấy Student để xóa
    }

    // Phương thức main để kiểm tra
    public static void main(String[] args) {
        LinkedListCollection collection = new LinkedListCollection();

        // Tạo một số đối tượng Student
        Student student1 = new Student(1, "Duong Truong", 20);
        Student student2 = new Student(2, "Huy Vu", 22);
        Student student3 = new Student(3, "Phuong Nguyen", 18);
        Student student4 = new Student(4, "Huy Vu", 18);
        Student student5 = new Student(5, "Ngoc Pham", 30);

        // Thêm các học sinh vào collection
        collection.addStudent(student1);
        collection.addStudent(student2);
        collection.addStudent(student3);
        collection.addStudent(student4);
        collection.addStudent(student5);

        // Sắp xếp
        Collections.sort(collection.studentList, new StudentSort());

        // In ra tất cả các Student trong danh sách
        System.out.println("Danh sách các Student:");
        collection.readAll();
        System.out.println();

        // Cập nhật thông tin của một Student có id=2
        Student updatedStudent = new Student(2, "Jane Smith", 23);
        boolean updateResult = collection.update(updatedStudent);
        if (updateResult) {
            System.out.println("Cập nhật thành công Student có id=2:");
            collection.readAll();
        } else {
            System.out.println("Không tìm thấy Student có id=2 để cập nhật.");
        }
        System.out.println();

        // Xóa một Student có id=1
        boolean deleteResult = collection.delete(1);
        if (deleteResult) {
            System.out.println("Xóa thành công Student có id=1:");
            collection.readAll();
        } else {
            System.out.println("Không tìm thấy Student có id=1 để xóa.");
        }
    }

}
