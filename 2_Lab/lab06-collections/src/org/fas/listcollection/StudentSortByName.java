package org.fas.listcollection;

import org.fas.model.Student;

import java.util.Comparator;

// Comparator để so sánh Student theo tên
public class StudentSortByName implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
