# String và char

Trong ngôn ngữ lập trình Java, String và char đều là kiểu dữ liệu 
để đại diện cho chuỗi ký tự, nhưng chúng có những đặc điểm khác nhau.

## I. Tổng Quan

### 1. char

- char là kiểu dữ liệu cơ bản trong Java, đại diện cho một ký tự Unicode 16-bit.
- char chỉ có thể lưu giữ một ký tự duy nhất, trong khi String có thể lưu trữ một chuỗi ký tự.
- char được khai báo bằng cách sử dụng dấu nháy đơn.
- Đối với char, bạn chỉ có thể gán một ký tự duy nhất.
- Giá trị mặc định cho char là ký tự null ('\u0000').

Ví dụ:

```
char ch = 'A';
```

### 2. String

![alt](https://images.viblo.asia/f3625ca6-d760-46be-8742-62110da8184a.png)

- String là một lớp trong Java, không phải là kiểu dữ liệu cơ bản. Nó là một đối tượng và được xây dựng từ lớp String.
- String là chuỗi ký tự không thay đổi (immutable). Điều này có nghĩa là một khi bạn đã tạo một đối tượng String, bạn không thể thay đổi nó. Mọi thao tác trên chuỗi sẽ tạo ra một đối tượng mới.
- String hỗ trợ nhiều phương thức để thực hiện các thao tác trên chuỗi như nối, cắt, so sánh, và tìm kiếm.
- Đối với String, bạn có thể gán một chuỗi ký tự.
- Giá trị mặc định cho String là null.
- String được coi là chuỗi ký tự không thay đổi (immutable). Điều này có nghĩa là một khi bạn đã tạo một đối tượng String, bạn không thể thay đổi nó. Thay vào đó, bất kỳ thao tác nào cũng sẽ tạo ra một đối tượng String mới, giữ nguyên đối tượng String ban đầu.

Ví dụ:

```
String greeting = "Hello";
```

### 3. String Pool

String Pool là một khu vực đặc biệt trong bộ nhớ Heap của Java được sử dụng để lưu trữ 
chuỗi (String) duy nhất. Khi bạn tạo một chuỗi bằng cách sử dụng cú pháp 
`String myString = "example";`, JVM kiểm tra xem chuỗi đã tồn tại trong String Pool chưa. 
Nếu chuỗi đó đã tồn tại, thì tham chiếu đến chuỗi đó được trả về thay vì tạo một 
đối tượng chuỗi mới. Nếu chuỗi chưa tồn tại, nó sẽ được tạo và thêm vào String Pool để 
sử dụng trong tương lai.

Lợi ích của String Pool bao gồm:
- Tiết Kiệm Bộ Nhớ: Bởi vì chuỗi duy nhất được lưu trữ chỉ một lần trong String Pool, nên việc sử dụng nhiều chuỗi giống nhau giữa các phần của chương trình không dẫn đến sự lãng phí bộ nhớ.
- Tối Ưu Hóa Hiệu Suất So Sánh: So sánh tham chiếu trực tiếp bằng toán tử == trong String Pool có thể nhanh hơn so sánh bằng equals() vì nó không cần so sánh từng ký tự.
- Sử Dụng Cho Interning: Phương thức `intern()` của lớp String cho phép đối tượng chuỗi được thêm vào String Pool nếu nó chưa tồn tại. Điều này hữu ích khi bạn muốn chắc chắn rằng một đối tượng chuỗi cụ thể sẽ được sử dụng trong String Pool.

## II. Thực Hiện So sánh

Trong Java, == và equals() là hai phương thức được sử dụng để so sánh đối tượng, nhưng chúng có mục đích và cách hoạt động khác nhau:

### 1. == (So sánh Đối Tượng)

- == kiểm tra xem hai tham chiếu đối tượng có trỏ đến cùng một vùng bộ nhớ hay không.
- Nếu == được sử dụng để so sánh hai biến tham chiếu, nó sẽ kiểm tra xem cả hai đang tham chiếu đến cùng một đối tượng hay không.

Ví dụ:

```
String str1 = new String("Hello");
String str2 = new String("Hello");

System.out.println(str1 == str2);  // false, vì đây là hai đối tượng khác nhau
```

### 2. equals() (So sánh Giá Trị)

- equals() là một phương thức được định nghĩa trong lớp Object và được ghi đè bởi nhiều lớp con, chẳng hạn như String.
- equals() thường được sử dụng để so sánh giá trị thực của hai đối tượng, thay vì so sánh tham chiếu.
- equalsIgnoreCase là một phương thức của lớp String trong Java được sử dụng để so sánh hai chuỗi mà không phân biệt chữ hoa chữ thường. 

Ví dụ:

```
String str1 = new String("Hello");
String str2 = new String("Hello");

System.out.println(str1.equals(str2));  // true, vì giá trị của hai chuỗi là giống nhau
```

## III. Bộ nhớ Head và Stack

Heap và Stack là hai phần quan trọng của bộ nhớ được sử dụng để quản lý dữ liệu và đối tượng trong quá trình thực thi chương trình.

Bộ nhớ Heap và bộ nhớ Stack bản chất đều cùng là vùng nhớ được tạo ra và lưu trữ trong RAM khi chương trình được thực thi.

![alt](https://www.baeldung.com/wp-content/uploads/2018/07/java-heap-stack-diagram.png)

### 1. Heap

- Bất cứ khi nào ở đâu trong chương trình của bạn khi bạn tạo Object thì nó sẽ được lưu trong Heap (thực thi toán tử new).
- Dung lượng Heap thường lớn hơn Stack.
- Thời gian sống của object phụ thuộc vào Garbage Collection, Garbage Collection sẽ chạy trên bộ nhớ Heap để xoá các Object không được sử dụng nữa, nghĩa là object không được referece trong chương trình.
- Khi Heap bị đầy chương trình hiện lỗi java.lang.OutOfMemoryError
- Sử dụng -Xms và -Xmx để định nghĩa dung lượng bắt đầu và dung lượng tối đa của bộ nhớ heap.

## 2. Stack

- Chỉ lưu trữ các biến nguyên thủy (primitive) và tham chiếu (references) đến các đối tượng được tạo trong Heap.
- Bộ nhớ stack thường nhỏ.
- Có thời gian tồn tại ngắn, Khi hàm thực hiện xong, khối bộ nhớ cho hàm sẽ bị xoá, và giải phóng bộ nhớ trong stack.
- Khi stack bị đầy bộ nhớ, chương trình phát sinh lỗi: java.lang.StackOverFlowError
- Dùng -Xss để định nghĩa dung lượng bộ nhớ stack.
