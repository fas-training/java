package exercise;

import java.util.Scanner;

public class MultipleStringsInput {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder inputBuilder = new StringBuilder();

        System.out.println("Enter strings (type 'exit' to finish input):");
        while (true) {
            String input = scanner.nextLine();

            if (input.equalsIgnoreCase("exit")) {
                break;
            }

            inputBuilder.append(input).append("\t");
        }

        System.out.println("Entered strings:\n" + inputBuilder.toString());

        scanner.close();
    }
}
