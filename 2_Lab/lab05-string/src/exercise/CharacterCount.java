package exercise;

import java.util.Scanner;

public class CharacterCount {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter a string: ");
        String inputString = scanner.nextLine();

        int letterCount = 0;
        int digitCount = 0;
        int spaceCount = 0;
        int specialCharCount = 0;

        for (int i = 0; i < inputString.length(); i++) {
            char ch = inputString.charAt(i);
            if (Character.isLetter(ch)) {
                letterCount++;
            } else if (Character.isDigit(ch)) {
                digitCount++;
            } else if (Character.isWhitespace(ch)) {
                spaceCount++;
            } else {
                specialCharCount++;
            }
        }

        System.out.println("Number of letters: " + letterCount);
        System.out.println("Number of digits: " + digitCount);
        System.out.println("Number of spaces: " + spaceCount);
        System.out.println("Number of special characters: " + specialCharCount);

        scanner.close();
    }

}
