package exercise;

import java.util.Scanner;

public class NameConversion {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean exitFlag = false;

        while (!exitFlag) {
            System.out.println("Enter your full name (Last Name First Name) or type 'EXIT' to quit: ");
            String fullName = scanner.nextLine().trim();

            if (fullName.equalsIgnoreCase("EXIT")) {
                exitFlag = true;
            } else {
                int spaceIndex = fullName.indexOf(' ');
                if (spaceIndex != -1) {
                    String firstName = fullName.substring(spaceIndex + 1);
                    String lastName = fullName.substring(0, spaceIndex);
                    String convertedName = firstName + " " + lastName;

                    System.out.println("Name after conversion: " + convertedName);
                } else {
                    System.out.println("Incorrect format. Please try again!");
                }
            }
        }

        scanner.close();
    }

}
