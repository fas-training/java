package exam;

import java.util.ArrayList;
import java.util.List;

public class ArrayExam {
    public static void main(String[] args) {
        ArrayList<Student> myArray = new ArrayList();

        myArray.add(new Student(1, "Duong"));
        myArray.add(new Student(2, "Huy"));
        myArray.add(null);

        System.out.println(myArray.size());
        System.out.println("index1: " + myArray.get(1));

        for (int i = 0; i < myArray.size(); i++) {
            Student s = myArray.get(i);
        }

        for (Student s : myArray) {
            System.out.println(s);
        }
    }
}
