package practice;

import java.util.Scanner;

public class StringValidator {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter a string to check its format (e.g., ABC-12345):");
        String userInput = scanner.nextLine();

        if (checkFormat(userInput)) {
            System.out.println("The string conforms to the format ABC-12345.");
        } else {
            System.out.println("The string does not conform to the specified format.");
        }

        scanner.close();
    }

    public static boolean isValidFormat(String input) {
        // Regular expression pattern to match the format ABC-12345
        String pattern = "^[A-Za-z]{3}-\\d{5}$";

        return input.matches(pattern);
    }

    private static boolean checkFormat(String input) {
        if (input.length() != 9) {
            return false;
        }

        // Kiểm tra ký tự 1-3 là chuỗi
        for (int i = 0; i < 3; i++) {
            if (!Character.isLetter(input.charAt(i))) {
                return false;
            }
        }

        // Kiểm tra ký tự 4 là dấu '-'
        if (input.charAt(3) != '-') {
            return false;
        }

        // Kiểm tra ký tự 5-9 là số
        for (int i = 4; i < input.length(); i++) {
            if (!Character.isDigit(input.charAt(i))) {
                return false;
            }
        }

        // Nếu qua tất cả kiểm tra, trả về true
        return true;
    }

}
