package practice;

import java.util.Scanner;

public class EmailValidator {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter an email address to validate: ");
        String emailAddress = scanner.nextLine();

        if (isValidEmail(emailAddress)) {
            System.out.println("Valid email address.");
        } else {
            System.out.println("Invalid email address.");
        }

        scanner.close();
    }

    public static boolean isValidEmail(String email) {
        // Regular expression pattern for a simple email validation
        String emailPattern = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$";

        return email.matches(emailPattern);
    }

}
