# JAVA Class and Object

## Nội Dung

- Class và Object 
- Khởi tạo Class và Object
- Phạm vi truy cập (Access Modifiers)
- Từ khoá static, final
- Heap Space và Stack Memory

## 1. Class và Object

![Class và Object](https://4.bp.blogspot.com/-YDN3HuXGY1g/WUpxo02n6iI/AAAAAAAAAE0/-5hQn9VZVL4s9bYhyyobfcFZKWxxIIEzACLcBGAs/s1600/cars.jpg)

### 1.1 Class là gì?

Trong Java, một Class (lớp) là một bản thiết kế (**blueprint**) hoặc một khuôn mẫu để tạo ra các đối tượng (Object). Nó định nghĩa các thuộc tính (**field**) và các phương thức (**method**) mà các đối tượng của lớp đó sẽ có

### 1.2 Object là gì?

Object (đối tượng) là một thể hiện (**instance**) của một lớp. Bạn có thể tạo nhiều thể hiện của một lớp.

Mỗi đối tượng sẽ có trạng thái (**state**) và hành vi (**behavior**). 

**Tóm lại**:
- **Class (Lớp)**: Là một bản thiết kế để tạo ra các đối tượng, chứa các thuộc tính và phương thức.
- **Object (Đối tượng)**: Là một thể hiện cụ thể của một lớp, có thể tương tác và sử dụng các thuộc tính và phương thức của lớp đó.

### 1.3 Ví dụ

Giả sử bạn có một lớp `Car` như sau:

```java
public class Car {
    String model;
    int year;
    double price;

    public Car(String model, int year, double price) {
        this.model = model;
        this.year = year;
        this.price = price;
    }

    public void displayDetails() {
        System.out.println("Model: " + model);
        System.out.println("Year: " + year);
        System.out.println("Price: $" + price);
    }
}
```

Bạn có thể tạo các đối tượng từ lớp Car và sử dụng chúng như sau:

```java
public class Main {
    public static void main(String[] args) {
        // Tạo đối tượng Car
        Car toy = new Car("Toyota Camry", 2021, 24000);
        Car hon = new Car("Honda Accord", 2020, 22000);

        // Hiển thị thông tin chi tiết của các đối tượng Car
        toy.displayDetails();
        hon.displayDetails();
    }
}
```

## 2. Khởi tạo Class và Object

### 2.1 Khởi tạo Class 

Tạo lớp mới bằng từ khóa `class`.

Một lớp có thể chứa:
- Các thuộc tính / trường (`instance variables`, `attribute`, `fields`)
- Các phương thức khởi tạo (`constructors`)
- Các phương thức (`instance method`, `static method`)

Dưới đây là một ví dụ về lớp `Person`, bao gồm các thuộc tính là mã (`id`), tên (`name`) và tuổi (`age`), cùng với các phương thức khởi tạo và các phương thức để lấy và hiển thị thông tin:

```java
public class Person {
    // Biến thể hiện
    private int id;
    private String name;
    private int age;

    // Phương thức khởi tạo (constructor)
    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    // Getter cho thuộc tính id
    public int getId() {
        return id;
    }

    // Setter cho thuộc tính id
    public void setId(int id) {
        this.id = id;
    }

    // Getter cho thuộc tính name
    public String getName() {
        return name;
    }

    // Setter cho thuộc tính name
    public void setName(String name) {
        this.name = name;
    }

    // Getter cho thuộc tính age
    public int getAge() {
        return age;
    }

    // Setter cho thuộc tính age
    public void setAge(int age) {
        this.age = age;
    }

    // Phương thức hiển thị thông tin của Person
    public void displayInfo() {
        System.out.println("ID: " + id);
        System.out.println("Name: " + name);
        System.out.println("Age: " + age);
    }
}
```

### 2.2 Khởi tạo Object 

Trong Java, để khởi tạo một đối tượng (object), bạn cần thực hiện các bước sau:
- Khai báo một biến tham chiếu đến đối tượng.
- Sử dụng từ khóa new để khởi tạo đối tượng.
- Gọi constructor của lớp để khởi tạo đối tượng.

Tạo lớp `main` để khởi tạo các đối tượng:

```java
public class Main {
    public static void main(String[] args) {
        // Tạo đối tượng Person
        Person person1 = new Person(1, "Alice", 30);
        
        // Sử dụng các phương thức của đối tượng Person
        person1.displayInfo();

        // Thay đổi giá trị thuộc tính thông qua setter
        person1.setAge(31);
        
        // Hiển thị thông tin sau khi thay đổi
        person1.displayInfo();
    }
}
```

### 2.3 Constructors

Constructor là một phương thức đặc biệt được sử dụng để khởi tạo các đối tượng. Constructor có tên giống với tên của lớp và không có kiểu trả về.

Constructor có thể có tham số để khởi tạo các giá trị ban đầu cho đối tượng, hoặc không có tham số (default constructor).

### 2.4 Method

![](https://media.geeksforgeeks.org/wp-content/uploads/methods-in-java.png)

Method (phương thức) trong Java là một khối mã được định nghĩa trong một lớp và thực hiện một nhiệm vụ cụ thể.

Một phương thức trong Java bao gồm:
- **Kiểu trả về**: Loại dữ liệu mà phương thức trả về. Nếu phương thức không trả về giá trị nào, sử dụng từ khóa void.
- **Tên phương thức**: Tên của phương thức, theo quy tắc đặt tên của Java.
- **Danh sách tham số**: Các tham số đầu vào của phương thức, nếu có, được đặt trong dấu ngoặc đơn.
- **Thân phương thức**: Khối mã thực hiện nhiệm vụ của phương thức, nằm trong cặp dấu ngoặc nhọn {}.

### 2.5 Access Modifiers

![Access Modifiers](https://simplesnippets.tech/wp-content/uploads/2018/04/access-modifiers-in-java.jpg)

Access modifiers giúp quản lý khả năng truy cập và bảo vệ dữ liệu trong các ứng dụng Java. Chúng giúp ngăn chặn việc truy cập trái phép và duy trì tính toàn vẹn của dữ liệu.

Có 4 loại Access Modifier (phạm vi truy cập):
- **public**: Có phạm vi truy cập rộng nhất, cho phép truy cập từ bất kỳ đâu.
- **private**: Có phạm vi truy cập hẹp nhất, chỉ cho phép truy cập từ bên trong cùng một lớp.
- **protected**: Cho phép truy cập từ bên trong cùng một package hoặc từ các lớp con (subclasses) ở bất kỳ package nào khác.
- **default**: Có phạm vi truy cập trung bình, cho phép truy cập từ bên trong cùng một package, nhưng không thể truy cập từ các package khác.

![Access Modifiers](https://images.viblo.asia/3efc9ede-d8e3-4b93-b1f0-7b46e1e773f3.png)

## 3. Từ khoá static, final

### 3.1 Từ khoá static

Từ khóa static được sử dụng để khai báo các biến, phương thức, hoặc khối mã của một lớp mà không phụ thuộc vào bất kỳ đối tượng cụ thể nào của lớp đó.

![static keyword](https://static.javatpoint.com/images/java-static-keyword1.png)

Từ khóa static thuộc về lớp chứ không thuộc về instance (thể hiện) của lớp.

Từ khóa static áp dụng cho các thành phần như:
- Biến (Variables)
- Phương thức (Methods)
- Khối (Blocks)
- Lớp (Classes)

Lý do sử dụng biến static
Chia sẻ dữ liệu giữa các đối tượng: Biến static trong Java được chia sẻ giữa tất cả các đối tượng của một lớp, thay vì mỗi đối tượng có một bản sao riêng biệt của biến. Điều này hữu ích khi muốn các đối tượng cùng truy cập và cập nhật một giá trị chung.
Định nghĩa các hằng số: Biến static kết hợp với từ khóa final thường được sử dụng để định nghĩa các hằng số, những giá trị không thay đổi trong suốt quá trình thực thi chương trình.
Tiết kiệm bộ nhớ: Khi sử dụng biến static, chỉ có một bản sao của biến đó được tạo ra trong bộ nhớ, không phụ thuộc vào số lượng đối tượng của lớp. Điều này giúp tiết kiệm bộ nhớ, đặc biệt khi có nhiều đối tượng cần chia sẻ cùng một giá trị.

#### 3.1.1 Biến tĩnh (Static Variables)

Chỉ có một bản sao duy nhất của biến tĩnh được tạo ra, và nó được chia sẻ giữa tất cả các đối tượng của lớp. Như vậy, bất kể có khởi tạo bao nhiêu đối tượng của một lớp, sẽ luôn chỉ có một bản sao của biến static thuộc về lớp đó.

Biến static được lưu trữ trong vùng nhớ heap.

Biến tĩnh có thể được truy cập trực tiếp thông qua tên lớp (ví dụ: `Student.variableName`).

```java
public class Example {
    static int staticVariable = 10;

    public static void main(String[] args) {
        System.out.println(Example.staticVariable); // Truy cập biến tĩnh thông qua tên lớp
    }
}
```

#### 3.1.2 Phương thức tĩnh (Static Methods)

Phương thức tĩnh (static methods) là các phương thức thuộc về lớp, không phải là các phương thức của đối tượng cụ thể. Điều này có nghĩa là bạn có thể gọi các phương thức tĩnh mà không cần tạo ra một đối tượng của lớp đó.

```java
public class Calculator {

    // Phương thức tính tổng của hai số
    public static int sum(int a, int b) {
        return a + b;
    }

    public static void main(String[] args) {
        // Gọi các phương thức tĩnh trực tiếp từ tên lớp
        System.out.println("Sum: " + Calculator.sum(5, 3));
    }
}
```

#### 3.1.3 Khối tĩnh (static block)

khối static" (static block) là một khối mã được sử dụng để thực hiện các hoạt động khởi tạo dành cho các thành viên tĩnh (static members) của lớp. Khối static được sử dụng để thực hiện các thao tác mà cần được thực hiện trước khi lớp được sử dụng hoặc trước khi tạo đối tượng của lớp đó.

Khối static chỉ được thực thi một lần khi lớp được nạp vào bộ nhớ (loaded into memory) lần đầu tiên. Điều này đảm bảo rằng các hoạt động khởi tạo chỉ xảy ra một lần duy nhất.

Các lệnh trong khối static thường được sử dụng để khởi tạo các biến static hoặc thực hiện các thao tác khác liên quan đến static.

```java
public class Person {
    // Biến static
    static int numOfObjects;

    static {
        // Khối static để khởi tạo biến static
        numOfObjects = 10;
        System.out.println("Static block initialized.");
    }

    public static void main(String[] args) {
        // Gọi biến static từ phương thức main
        System.out.println("Number: " + numOfObjects);
    }
}
```

#### 3.1.4 Lớp tĩnh (nested static class)

Lớp tĩnh (nested static class) là một lớp lồng nhau (nested class) được đặt bên trong một lớp khác và được khai báo là static. Điều này có nghĩa là bạn có thể truy cập lớp tĩnh mà không cần tạo ra một đối tượng của lớp bao ngoài.

Lớp tĩnh thường được sử dụng để tổ chức mã của mình một cách logic, nhóm các lớp lại với nhau khi chúng có một mối quan hệ chặt chẽ và không cần phụ thuộc vào các đối tượng của lớp bao ngoài.

```java
public class School {
    // Lớp tĩnh lồng nhau biểu diễn sinh viên
    static class Student {
        private int studentId;
        private String name;

        // Constructor của lớp tĩnh
        public Student(int studentId, String name) {
            this.studentId = studentId;
            this.name = name;
        }

        // Phương thức hiển thị thông tin sinh viên
        public void display() {
            System.out.println("Student ID: " + studentId);
            System.out.println("Name: " + name);
        }
    }

    public static void main(String[] args) {
        // Tạo đối tượng của lớp tĩnh và hiển thị thông tin sinh viên
        Student student1 = new Student(1, "John");
        Student student2 = new Student(2, "Alice");

        System.out.println("Student 1:");
        student1.display();

        System.out.println("\nStudent 2:");
        student2.display();
    }
}
```

#### 3.1.5 Import static

Từ phiên bản JDK 5 trở đi, bạn có thể sử dụng câu lệnh "import static" để nhập các thành viên tĩnh (static members) của một lớp vào mã nguồn của bạn. Điều này giúp bạn có thể truy cập các phương thức và biến tĩnh mà không cần sử dụng tên lớp mỗi lần bạn muốn sử dụng chúng.

Dưới đây là một ví dụ về cách tạo lớp MathUtils trong gói com.example.utils, chứa một phương thức tĩnh là add và một biến tĩnh là PI:

```java
package com.example.utils;

public class MathUtils {
    // Biến tĩnh
    public static final double PI = 3.14159;

    // Phương thức tĩnh để thực hiện phép cộng
    public static int add(int a, int b) {
        return a + b;
    }
}
```

Dưới đây là một ví dụ về cách import các biến tĩnh từ lớp Math trong gói java.lang vào một lớp khác:

```java
import static com.example.utils.MathUtils.*;

public class Main {
    public static void main(String[] args) {
        // Gọi phương thức tĩnh add từ lớp MathUtils mà không cần sử dụng tên lớp MathUtils
        int sum = add(5, 3);
        System.out.println("Sum: " + sum);
    }
}
```
### 3.2 Từ khoá final

Từ khóa final được sử dụng để định nghĩa hằng số, khóa chống ghi đè (override), và khóa không thể thay đổi cho các biến, phương thức, và lớp.

![final keyword](https://prepbytes-misc-images.s3.ap-south-1.amazonaws.com/assets/1674814892299-Final%20Keyword%20in%20Java1.png)

Từ khóa final áp dụng cho các thành phần như:
- **Biến (variable)**: Khi một biến được khai báo là final, giá trị của nó không thể thay đổi sau khi đã được gán giá trị ban đầu.
- **Phương thức (method)**: Khi một phương thức được đánh dấu là final, nó không thể bị ghi đè (override) bởi lớp con.
- **Lớp (class)**: Khi một lớp được đánh dấu là final, nó không thể được kế thừa.
- **Tham số (parameter)**: Trong phương thức, khi một tham số được đánh dấu là final, giá trị của nó không thể thay đổi bên trong phương thức đó.

### 3.2.1 Biến final

Khi một biến được khai báo là final, giá trị của nó không thể thay đổi sau khi đã được gán giá trị ban đầu.

```java
final int MAX_VALUE = 100;
```

### 3.2.2 Phương thức final

Khi một phương thức được đánh dấu là final, nó không thể bị ghi đè (override) bởi lớp con.

```java
class Parent {
    final void display() {
        System.out.println("Parent's display method");
    }
}
```

### 3.2.3 Lớp final

Khi một lớp được đánh dấu là final, nó không thể được kế thừa.

```java
// Lớp FinalClass được đánh dấu là final, nên không thể bị kế thừa
public final class FinalClass {
    private int value;

    public FinalClass(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void display() {
        System.out.println("Value: " + value);
    }
}

// Lớp SubClass sẽ gây lỗi biên dịch vì nó cố gắng kế thừa từ lớp final
// class SubClass extends FinalClass {
//     public SubClass(int value) {
//         super(value);
//     }
// }

public class Main {
    public static void main(String[] args) {
        // Tạo đối tượng của lớp FinalClass
        FinalClass finalClassInstance = new FinalClass(10);
        finalClassInstance.display(); // In ra "Value: 10"

        // Thay đổi giá trị và hiển thị lại
        finalClassInstance.setValue(20);
        finalClassInstance.display(); // In ra "Value: 20"
    }
}
```

### 3.2.4 Tham số (parameter)

Khi một tham số được đánh dấu là final, giá trị của nó không thể thay đổi bên trong phương thức đó.

```java
void process(final int x) {
    // Giá trị của x không thể thay đổi
    System.out.println("Value of x: " + x);
}
```

## 4. Heap Space và Stack Memory

Bộ nhớ được chia thành hai vùng chính: Heap Space và Stack Memory. Cả hai vùng này đều được sử dụng để lưu trữ dữ liệu, nhưng chúng có mục đích và cách thức hoạt động khác nhau.

![Heap Space và Stack Memory](https://www.baeldung.com/wp-content/uploads/2018/07/java-heap-stack-diagram.png)

### 4.1 Heap Space

- **Chức năng**: Heap Space được sử dụng để lưu trữ các đối tượng trong Java. Bất kỳ thứ gì được tạo bằng từ khóa new đều được lưu trữ trong Heap.

- **Quản lý**: Bộ thu gom rác (Garbage Collector) quản lý Heap, tự động giải phóng bộ nhớ của các đối tượng không còn được tham chiếu.

- **Truy cập**: Các đối tượng trong Heap có thể được truy cập từ bất kỳ nơi nào trong chương trình.
Kích thước: Kích thước của Heap có thể được cấu hình và thường lớn hơn Stack.

- **Vòng đời**: Đối tượng trong Heap tồn tại cho đến khi không còn tham chiếu nào đến chúng, sau đó bộ thu gom rác sẽ thu hồi bộ nhớ.

```java
class Person {
    String name;
    int age;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }
}

public class Main {
    public static void main(String[] args) {
        // Tạo một đối tượng trong Heap Space
        Person person = new Person("John", 25);
    }
}
```

### 4.2 Stack Memory

- **hức năng**: Stack Memory được sử dụng để lưu trữ các biến cục bộ, các tham số của phương thức và các lời gọi hàm. Mỗi khi một phương thức được gọi, một khung (frame) mới được tạo ra trong Stack để lưu trữ các biến cục bộ của phương thức đó.

- **Quản lý**: Stack Memory được quản lý theo nguyên tắc LIFO (Last In, First Out). Mỗi khi một phương thức kết thúc, khung của nó sẽ bị loại bỏ khỏi Stack.

- **Truy cập**: Các biến cục bộ trong Stack chỉ có thể được truy cập từ trong phương thức nơi chúng được khai báo.
Kích thước: Kích thước của Stack thường nhỏ hơn Heap và có giới hạn cố định.

- **Vòng đời**: Biến cục bộ trong Stack tồn tại cho đến khi phương thức chứa chúng kết thúc.

```java
public class Main {
    public void display(int num) {
        // Biến cục bộ num và result được lưu trữ trong Stack
        int result = num * 2;
        System.out.println("Result: " + result);
    }

    public static void main(String[] args) {
        // Các biến cục bộ được lưu trữ trong Stack
        int number = 10;
        Main main = new Main();
        main.display(number);
    }
}
```

### 4.3 So sánh giữa Heap Space và Stack Memory

| Đặc điểm     | Heap Space                                       | Stack Memory                                   |
|--------------|--------------------------------------------------|------------------------------------------------|
| **Chức năng**| Lưu trữ các đối tượng                             | Lưu trữ các biến cục bộ, tham số của phương thức, và các lời gọi hàm |
| **Quản lý**  | Bộ thu gom rác (Garbage Collector)                | LIFO (Last In, First Out)                      |
| **Truy cập** | Từ bất kỳ nơi nào trong chương trình             | Chỉ trong phạm vi phương thức chứa             |
| **Kích thước**| Thường lớn hơn và có thể cấu hình               | Thường nhỏ hơn và có giới hạn cố định          |
| **Lifetime** | Đối tượng tồn tại cho đến khi không còn tham chiếu nào đến chúng | Biến tồn tại cho đến khi phương thức chứa kết thúc |
| **Đặc điểm** | Được sử dụng để cấp phát động (dynamic allocation) | Được sử dụng cho cấp phát tĩnh (static allocation) |
| **Tốc độ**   | Chậm hơn so với Stack do quản lý bởi Garbage Collector | Nhanh hơn do quản lý trực tiếp bằng LIFO       |
| **Khi nào sử dụng**| Khi tạo các đối tượng hoặc cấp phát bộ nhớ lớn | Khi khai báo các biến cục bộ và thực hiện lời gọi hàm |

=== THE END ===
