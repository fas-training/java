package vn.fas;

public class Main {

    static Person[] personList = new Person[5];

    // Khối static
    static {
        // Tạo các đối tượng Person
        Person person1 = new Person("Duong", 20);
        Person person2 = new Person("Huy", 25);

        personList[0] = person1;
        personList[1] = person2;
    }

    public static void main(String[] args) {
        Person person1 = personList[0];
        Person person2 = personList[1];

        // Hiển thị thông tin của các đối tượng
        person1.displayInfo();
        person2.displayInfo();

        person2.setNumOfObjects(10);

        System.out.println(person1.getNumOfObjects());
        System.out.println(person2.getNumOfObjects());

        // Hiển thị tổng số đối tượng Person đã được tạo
        System.out.println("Total number of Person objects: " + Person.getNumOfObjects());

        // Duyệt qua phần tử trong mảng
        if (personList != null) {
            for (Person person : personList) {
                person.displayInfo();
            }
        }
    }

}