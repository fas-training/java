package vn.fas;

public class Person {

    // Biến tĩnh
    private static int numOfObjects = 0;

    // Các biến thể hiện (instance)
    private int personId;
    private String name;
    private int age;

    // Constructor không tham số (default constructor)
    public Person() {
        this.personId = ++numOfObjects; // Tự động tăng personId dựa trên biến tĩnh numOfObjects
    }

    // Constructor có tham số
    public Person(String name, int age) {
        this.personId = ++numOfObjects; // Tự động tăng personId dựa trên biến tĩnh numOfObjects
        this.name = name;
        this.age = age;
    }

    // Getter và Setter cho các thuộc tính
    public int getPersonId() {
        return personId;
    }

    // Getter và Setter cho các thuộc tính
    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    // Phương thức hiển thị thông tin của Person
    public void displayInfo() {
        System.out.println("Person ID: " + personId);
        System.out.println("Name: " + name);
        System.out.println("Age: " + age);
    }

    // Phương thức tĩnh để lấy tổng số đối tượng Person đã được tạo
    public static int getNumOfObjects() {
        return numOfObjects;
    }

    // Phương thức tĩnh để thiết lập tổng số đối tượng Person
    public void setNumOfObjects(int numOfObjects) {
        Person.numOfObjects = numOfObjects;
    }

    // Lớp tĩnh
    public static class Address {
        private String street;
        private String city;
        private String country;

        // Constructor của lớp lồng nhau tĩnh
        public Address(String street, String city, String country) {
            this.street = street;
            this.city = city;
            this.country = country;
        }

        // Phương thức hiển thị thông tin địa chỉ
        public void displayAddress() {
            System.out.println("Address: " + street + ", " + city + ", " + country);
        }
    }

}
