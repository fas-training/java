package org.example;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ObjectStreamExample {

    public void readFile(String path) {
        try {
            // Tạo một đối tượng FileInputStream
            FileInputStream fis = new FileInputStream(path);

            ObjectInputStream ois = new ObjectInputStream(fis);

            List<Person> personList = new ArrayList<>();

            // Đọc dữ liệu từ file và chuyển đổi nó thành một đối tượng Java tương ứng.
            Object obj = ois.readObject();

            // instanceof là một toán tử được sử dụng để kiểm tra xem một đối tượng có phải là một thể hiện của một lớp hay không.
            if (obj instanceof List) {
                personList = (List<Person>) obj;
                System.out.println("PersonList: " + personList);
            }

            // Kiểm tra xem đối tượng có phải là một Person hay không
            if (obj instanceof Person) {
                Person person = (Person) obj;
                System.out.println("Person: " + person);
            }

            // Đóng sau khi sử dụng xong
            fis.close();
            ois.close();
        } catch (FileNotFoundException e) {
            // Xử lý ngoại lệ nếu file không tồn tại
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            // Xử lý ngoại lệ nếu có lỗi xảy ra trong quá trình đọc file
            e.printStackTrace();
        }
    }

    public void writeFile(String path) {
        try {
            // Tạo một đối tượng FileOutputStream
            FileOutputStream fos = new FileOutputStream(path);

            ObjectOutputStream oos = new ObjectOutputStream(fos);

            // Ghi danh sách đối tượng Person vào file
            List<Person> personList = new ArrayList<>();
            personList.add(new Person("dương", 25));
            personList.add(new Person("huy", 30));

            // Ghi đối tượng vào file
            oos.writeObject(personList);

            System.out.println("Done");

            // Đóng sau khi sử dụng xong
            fos.close();
        } catch (FileNotFoundException e) {
            // Xử lý ngoại lệ nếu file không tồn tại
            e.printStackTrace();
        } catch (IOException e) {
            // Xử lý ngoại lệ nếu có lỗi xảy ra trong quá trình đọc file
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        String filePath = "/Users/duong/Data/CODE/object_file.txt";

        ObjectStreamExample objectStream = new ObjectStreamExample();

        // Ghi tập tin
        objectStream.writeFile(filePath);

        // Đọc tập tin
        objectStream.readFile(filePath);
    }

}
