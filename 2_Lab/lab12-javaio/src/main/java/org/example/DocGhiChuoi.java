package org.example;

import java.io.*;

public class DocGhiChuoi {

    public void ghiFile(String path, String data, boolean append) {
        FileWriter file = null;
        try {
            file = new FileWriter(path, append);
            BufferedWriter writer = new BufferedWriter(file);

            // ghi du lieu
            writer.write(data);
            writer.newLine();

            // day du lieu di
            writer.flush();

            file.close();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void docFile(String path) {
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(path);
            BufferedReader reader = new BufferedReader(fileReader);

            String data;
            while ((data = reader.readLine()) != null) {
                System.out.println(data);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        String path = "/Users/duong/Data/CODE/exam_string.txt";
        String data = "duong";

        DocGhiChuoi docGhiChuoi = new DocGhiChuoi();
        docGhiChuoi.ghiFile(path, data, true);
        docGhiChuoi.ghiFile(path, data, true);
        docGhiChuoi.ghiFile(path, data, true);
        System.out.println("luu thanh cong");


        docGhiChuoi.docFile(path);
        System.out.println("doc file thanh cong");
    }


}
