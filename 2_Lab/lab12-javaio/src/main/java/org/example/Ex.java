package org.example;

import java.util.Scanner;

public class Ex {
    public static void main(String[] args) {

        // tạo array[3][3];
        int[][] arrayTTT = new int[3][3];
        Scanner scanner = new Scanner(System.in);

        //cho giá trị của Array tương ứng như bảng
        int a = 1;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                arrayTTT[i][j] = a;
                a++;
            }
        }
        // In bảng Tic Tac Toe ra
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(arrayTTT[i][j] + " | ");
            }
            System.out.println("");
        }

        int numberOfMoves;
        int player1Move;
        int player2Move;
        int check = 0;
        for (numberOfMoves = 0; numberOfMoves < 9; numberOfMoves++) {
            if (numberOfMoves % 2 == 0) { //Đến lượt player 1
                System.out.println("Đến lượt player 1 nhe, chọn đi. ");
                while (check == 0){ //Kiểm tra Player 1 chọn đúng không
                    player1Move = scanner.nextInt();
                    for (int i = 0; i < 3; i++) {
                        for (int j = 0; j < 3; j++) {
                            if (player1Move == arrayTTT[i][j]) {
                                arrayTTT[i][j] = 0;
                                check = 1;
                            }
                        }
                    }
                    if (check == 0){
                        System.out.println("Nhập lại đi Player 1, chọn trùng rồi.");
                    }
                }
                check = 0;
            } else {//Đến lượt player 2
                System.out.println("Đến lượt player 2 nhe, chọn đi. ");
                while (check == 0){ //Kiểm tra Player 1 chọn đúng không
                    player2Move = scanner.nextInt();
                    for (int i = 0; i < 3; i++) {
                        for (int j = 0; j < 3; j++) {
                            if (player2Move == arrayTTT[i][j]) {
                                arrayTTT[i][j] = 0;
                                check = 1;
                            }
                        }
                    }
                    if (check == 0){
                        System.out.println("Nhập lại đi Player 2, chọn trùng rồi.");
                    }
                }
                check = 0;
            }
        }
        System.out.println("Kết thúc game rùi nhe.");
    }
}
