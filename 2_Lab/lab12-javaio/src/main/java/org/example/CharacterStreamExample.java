package org.example;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CharacterStreamExample {

    public void writeFile(String path) {
        try {
            PrintWriter printWriter = new PrintWriter(path);

            printWriter.println("Hello, FA!");
            printWriter.println("I'm Duong");

            FileWriter fileWriter = new FileWriter(path);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write("Hello, FA!");
            bufferedWriter.newLine();
            bufferedWriter.write("I'm Duong");
            bufferedWriter.flush();
            bufferedWriter.close();

            System.out.println("Done");

            // Đóng sau khi sử dụng xong
            printWriter.close();
        } catch (IOException e) {
            // Xử lý ngoại lệ nếu có lỗi xảy ra trong quá trình đọc file
            e.printStackTrace();
        }
    }

    public void readFile(String path) {
        try {
            FileReader fileReader = new FileReader(path);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }

            fileReader.close();
            bufferedReader.close();
        } catch (IOException e) {
            // Xử lý ngoại lệ nếu có lỗi xảy ra trong quá trình đọc file
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        String filePath = "/Users/duong/Data/CODE/character_file.txt";

        CharacterStreamExample characterStream = new CharacterStreamExample();

        // Ghi tập tin
        characterStream.writeFile(filePath);

        // Đọc tập tin
        characterStream.readFile(filePath);
    }

}
