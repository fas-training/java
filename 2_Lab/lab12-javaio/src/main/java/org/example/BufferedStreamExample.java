package org.example;

import java.io.*;

public class BufferedStreamExample {

    public void readFile(String path) {
        try {
            FileInputStream fis = new FileInputStream(path);
            BufferedInputStream bis = new BufferedInputStream(fis);

            // Đọc dữ liệu từ file sử dụng BufferedInputStream
            int byteData;

            // Đọc dữ liệu từ file và in ra màn hình
            while ((byteData = fis.read()) != -1) {
                System.out.print((char) byteData);
            }

            // Đóng sau khi sử dụng xong
            fis.close();
            bis.close();
        } catch (FileNotFoundException e) {
            // Xử lý ngoại lệ nếu file không tồn tại
            e.printStackTrace();
        } catch (IOException e) {
            // Xử lý ngoại lệ nếu có lỗi xảy ra trong quá trình đọc file
            e.printStackTrace();
        }
    }

    public void writeFile(String path) {
        try {
            // Tạo một đối tượng FileOutputStream
            FileOutputStream fos = new FileOutputStream(path);

            BufferedOutputStream bos = new BufferedOutputStream(fos);

            // Dữ liệu cần ghi vào file
            String data = "Hello,\tFA!\nI'm Duong";

            // Chuyển đổi chuỗi thành mảng byte và ghi vào file
            byte[] byteData = data.getBytes();

            // Ghi dữ liệu vào file sử dụng BufferedOutputStream
            bos.write(byteData);

            // Đảm bảo rằng tất cả dữ liệu trong bộ đệm được ghi vào file ngay lập tức
            bos.flush();

            System.out.println("Done");

            // Đóng sau khi sử dụng xong
            fos.close();
            bos.close();
        } catch (FileNotFoundException e) {
            // Xử lý ngoại lệ nếu file không tồn tại
            e.printStackTrace();
        } catch (IOException e) {
            // Xử lý ngoại lệ nếu có lỗi xảy ra trong quá trình đọc file
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        String inputPath = "/Users/duong/Data/CODE/input.txt";
        String outputPath = "/Users/duong/Data/CODE/output.txt";

        BufferedStreamExample bufferedStream = new BufferedStreamExample();

        // Đọc tập tin
        bufferedStream.readFile(inputPath);

        // Ghi tập tin
        bufferedStream.writeFile(outputPath);
    }

}
