package org.example;

import java.util.Scanner;

public class Problem2 {
    public static void main(String[] args) {
        // Khai báo mảng
        int[] numbers;

        // Gọi hàm để nhập mảng từ người dùng
        numbers = inputArray();

        // Gọi hàm để hiển thị và tính tổng số lượng số chẵn và lẻ
        displayAndSumEvenOdd(numbers);
    }

    // Hàm nhập mảng từ người dùng
    public static int[] inputArray() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter size of array: ");
        int size = scanner.nextInt();

        int[] array = new int[size];

        System.out.println("Enter array elements: ");
        for (int i = 0; i < size; i++) {
            System.out.print("Phần tử thứ " + (i + 1) + ": ");
            array[i] = scanner.nextInt();
        }

        scanner.close();

        return array;
    }

    // Hàm hiển thị và tính tổng số lượng số chẵn và lẻ
    public static void displayAndSumEvenOdd(int[] array) {
        int countEven = 0;
        int countOdd = 0;

        System.out.print("\nArray of elements: ");
        for (int number : array) {
            System.out.print(number + " ");

            // Kiểm tra và tính tổng số lượng số chẵn và lẻ
            if (number % 2 == 0) {
                countEven++;
            } else {
                countOdd++;
            }
        }

        System.out.println("\nNumber of even elements: " + countEven);
        System.out.println("Number of odd elements: " + countOdd);
    }
}
