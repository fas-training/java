package org.example;

public class Problem1 {
    public static void main(String[] args) {
        // cho mang numbers[]={11,12,13,14,15};
        // khai bao mang
        int[]  numbers={11,12,13,14,15};
        // goi ham va truyen tham so de dao nguoc mang
        Problem1(numbers);
        // in ra mang sau khi dao nguoc
        System.out.print("Output: ");
        // dung vong lap foreach
        for (int number : numbers)
        {
            System.out.print(number + "");
        }
    }
    // ham dao nguoc mang: truyen int[] numbers
    public static void Problem1(int[] numbers) {
        int start = 0;
        int end = numbers.length - 1;
        while (start < end) {
            // khai bao temp
            int temp = numbers[start];
            // tao doi tuong cho mang numbers[]
            numbers[start] = numbers[end];
            numbers[end] = temp;
            start++;
            end--;
        }
    }
}
