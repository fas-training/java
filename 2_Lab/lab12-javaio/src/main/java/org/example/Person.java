package org.example;

import java.io.Serializable;

// Triệt tiêu Serializable giúp Java biết cách chuyển đổi đối tượng Person thành dữ liệu nhị phân
// và ngược lại khi thực hiện các thao tác ghi và đọc.

// Nếu một lớp không triệt tiêu Serializable và bạn cố gắng sử dụng ObjectOutputStream và ObjectInputStream
// để ghi và đọc đối tượng của lớp đó, bạn sẽ gặp ngoại lệ NotSerializableException.
public class Person implements Serializable {
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{name='" + name + "', age=" + age + '}';
    }
}
