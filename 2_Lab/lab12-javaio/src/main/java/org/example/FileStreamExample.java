package org.example;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileStreamExample {

    public void readFile(String path) {
        try {
            // Tạo một đối tượng FileInputStream
            FileInputStream fis = new FileInputStream(path);

            //  Trả về một ước lượng về số lượng byte mà có thể đọc
            System.out.println("Bytes: " + fis.available());

            int byteData;

//            // Bỏ qua 2 byte từ vị trí hiện tại
//            fis.skip(2);

            // Đọc dữ liệu từ file và in ra màn hình
            while ((byteData = fis.read()) != -1) {
                System.out.print((char) byteData);
            }

            // Đóng sau khi sử dụng xong
            fis.close();
        } catch (FileNotFoundException e) {
            // Xử lý ngoại lệ nếu file không tồn tại
            e.printStackTrace();
        } catch (IOException e) {
            // Xử lý ngoại lệ nếu có lỗi xảy ra trong quá trình đọc file
            e.printStackTrace();
        }
    }

    public void writeFile(String path) {
        try {
            // Tạo một đối tượng FileOutputStream
            FileOutputStream fos = new FileOutputStream(path);

            // Dữ liệu cần ghi vào file
            String data = "Hello,\tFA!\nI'm Duong";

            // Chuyển đổi chuỗi thành mảng byte và ghi vào file
            byte[] byteData = data.getBytes();

            // Ghi dữ liệu
            fos.write(byteData);

            System.out.println("Done");

            // Đóng sau khi sử dụng xong
            fos.close();
        } catch (FileNotFoundException e) {
            // Xử lý ngoại lệ nếu file không tồn tại
            e.printStackTrace();
        } catch (IOException e) {
            // Xử lý ngoại lệ nếu có lỗi xảy ra trong quá trình đọc file
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        String inputPath = "/Users/duong/Data/CODE/input.txt";
        String outputPath = "/Users/duong/Data/CODE/output.txt";

        FileStreamExample fileStream = new FileStreamExample();

        // Đọc tập tin
//        fileStream.readFile(inputPath);

        // Ghi tập tin
        fileStream.writeFile(outputPath);
    }

}
