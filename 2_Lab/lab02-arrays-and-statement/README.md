# JAVA Arrays and Flow Control Statements

## Nội Dung

- Ép kiểu (Type Casting) 
- Mảng (Arrays)
- Flow Control Statements (if..else, switch-case, while, do-while, for)

## 1. Ép kiểu (Type Casting) 

### 1.1 Ép kiểu là gì?

- Ép kiểu trong Java là quá trình chuyển đổi giữa các kiểu dữ liệu khác nhau. Điều này thường được thực hiện khi bạn muốn gán giá trị từ một biến hoặc một biểu thức của một kiểu dữ liệu sang kiểu dữ liệu khác.
- Có hai cách chính để ép kiểu trong Java: Ép kiểu ngầm định (Implicit Casting) và Ép kiểu tường minh (Explicit Casting).

### 1.2 Ép kiểu ngầm định (tự động)

- Ép kiểu tự động xảy ra khi kiểu dữ liệu nhỏ hơn có thể tự động chuyển đổi sang kiểu dữ liệu lớn hơn mà không gây mất mát dữ liệu.
- Java thực hiện ép kiểu tự động một cách an toàn. 

Ví dụ:

```java
int intValue = 10;
double doubleValue = intValue; // Ép kiểu tự động từ int sang double
```

### 1.3 Ép kiểu tường minh (thủ công)

- Ép kiểu tường minh xảy ra khi bạn muốn chuyển đổi giữa hai kiểu dữ liệu khác nhau.
- Cần đảm bảo rằng việc chuyển đổi này an toàn (không gây mất mát dữ liệu).

Ví dụ:

```java
double doubleValue = 10.5;
int intValue = (int) doubleValue; // Ép kiểu tường minh từ double sang int
```

**Lưu ý**: Trong trường hợp của ép kiểu tường minh, có thể xảy ra mất mát dữ liệu nếu giá trị của biến không thể biểu diễn được trong kiểu dữ liệu đích. Vì vậy, hãy sử dụng ép kiểu tường minh cẩn thận và chỉ khi bạn chắc chắn rằng không có mất mát dữ liệu.

Ví dụ:

```java
int intValue = 100000; // Giá trị của biến int lớn hơn phạm vi của kiểu dữ liệu short
short shortValue = (short) intValue; // Ép kiểu int sang short
System.out.println("Int value: " + intValue); // 100000
System.out.println("Short value after casting: " + shortValue); //-31072
```

## 2. Mảng (Arrays)

### 2.1 Mảng là gì?

Mảng (Arrays) là cấu trúc dữ liệu được sử dụng để lưu trữ một tập hợp các phần tử cùng kiểu dữ liệu, liên tiếp nhau trong bộ nhớ. Mảng cho phép truy cập các phần tử thông qua chỉ số (index).

Chúng ta có thể lưu trữ giá trị nguyên thuỷ (primitive) hoặc đối tượng (object) trong một mảng trong Java.

Chúng ta cũng có thể tạo mảng một chiều hoặc đa chiều trong Java.

![Mảng (Arrays)](https://static.javatpoint.com/core/images/how-to-find-array-length-in-java2.png)

### 2.2 Khai báo mảng

Ba cách để khai báo một mảng là

```java
datatype[] newArray; // Chỉ khai báo một biến mảng, không khởi tạo
datatype[] newArray = new datatype[size]; // Khai báo và khởi tạo một mảng với kích thước của mảng
datatype[] newArray = {value1,value2,…valueN}; // Khai báo và khởi tạo một mảng với các giá trị ban đầu
```

Ví dụ:

```java
int[] newArray;
int[] newArray = new int[10];
int[] newArray = {1, 2, 3, 4, 5};
double[] doubleArray = {1.1, 2.2, 3.3, 4.4, 5.5};
char[] charArray = {'a', 'b', 'c', 'd', 'e'};
String[] stringArray = {"Hello", "World"};
```

### 2.3 Thực hiện thao tác mảng

Ví dụ cho mảng `numbers` như sau:

```java
int[] numbers = {1, 2, 3, 4, 5};
```

Truy cập phần tử của mảng:

```java
int firstElement = numbers[0]; // Truy cập phần tử đầu tiên (có chỉ số là 0)
int thirdElement = numbers[2]; // Truy cập phần tử thứ ba (có chỉ số là 2)
```

Thay đổi giá trị của phần tử của mảng:

```java
numbers[0] = 10; // Thay đổi giá trị của phần tử đầu tiên thành 10
numbers[3] = numbers[1] + numbers[2]; // Gán giá trị mới cho phần tử thứ tư dựa trên hai phần tử khác
```

Lặp qua mảng:

```java
for (int i = 0; i < numbers.length; i++) {
    System.out.println(numbers[i]); // In ra từng phần tử của mảng
}

// Sử dụng vòng lặp for-each
for (int num : numbers) {
    System.out.println(num); // In ra từng phần tử của mảng
}
```

## 3. Flow Control Statements

Câu lệnh `if-else` và `switch-case` là hai cấu trúc điều khiển chính trong Java, được sử dụng để thực thi các khối mã khác nhau dựa trên điều kiện.

### 3.1 If-Else

Câu lệnh if-else được sử dụng để kiểm tra một điều kiện và thực thi một khối mã nếu điều kiện đó đúng, và có thể thực thi một khối mã khác nếu điều kiện đó sai.

Cú pháp:

```java
if (condition) {
    // Thực thi nếu điều kiện đúng
} else if (anotherCondition) {
    // Thực thi nếu điều kiện khác đúng
} else {
    // Thực thi nếu tất cả điều kiện đều sai
}
```

Ví dụ:

```java
public class IfElseExample {
    public static void main(String[] args) {
        int number = 10;

        if (number > 0) {
            System.out.println("The number is positive.");
        } else if (number < 0) {
            System.out.println("The number is negative.");
        } else {
            System.out.println("The number is zero.");
        }
    }
}
```
### 3.2 Switch-Case

Mệnh đề switch-case được sử dụng để thực thi 1 hoặc nhiều khối lệnh từ nhiều điều kiện.

Cú pháp:

```java
switch (expression) {
    case value1:
        // Thực thi nếu biểu thức bằng value1
        break;
    case value2:
        // Thực thi nếu biểu thức bằng value2
        break;
    // Các trường hợp khác
    default:
        // Thực thi nếu không có trường hợp nào khớp
        break;
}
```

Ví dụ:

```java
public class SwitchCaseExample {
    public static void main(String[] args) {
        int day = 3;
        String dayName;

        switch (day) {
            case 1:
                dayName = "Monday";
                break;
            case 2:
                dayName = "Tuesday";
                break;
            case 3:
                dayName = "Wednesday";
                break;
            case 4:
                dayName = "Thursday";
                break;
            case 5:
                dayName = "Friday";
                break;
            case 6:
                dayName = "Saturday";
                break;
            case 7:
                dayName = "Sunday";
                break;
            default:
                dayName = "Invalid day";
                break;
        }

        System.out.println("The day is: " + dayName);
    }
}
```

**Sử dụng If-Else**:
- Thích hợp khi có các điều kiện phức tạp, hoặc khi các điều kiện không đơn thuần là so sánh giá trị của một biến.
- Có thể xử lý các điều kiện khác nhau như lớn hơn, nhỏ hơn, bằng, không bằng, và các phép so sánh phức tạp khác.

**Sử dụng Switch-Case**:
- Thích hợp khi có một biến và bạn muốn so sánh biến đó với nhiều giá trị cụ thể.
Dễ đọc và bảo trì hơn khi có nhiều giá trị so sánh cụ thể.
- Chỉ hỗ trợ các kiểu dữ liệu nguyên thủy như int, char, byte, short và enum, và String.

### 3.3 While

Vòng lặp while lặp lại một khối lệnh chừng nào điều kiện kiểm tra vẫn đúng. Điều kiện được kiểm tra trước khi thực hiện khối lệnh, nên nếu điều kiện sai ngay từ đầu, khối lệnh sẽ không được thực hiện lần nào.

Cú pháp:

```java
while (condition) {
    // Khối lệnh sẽ được thực hiện nếu condition là true
}
```

Ví dụ:

```java
public class WhileLoopExample {
    public static void main(String[] args) {
        int i = 0;
        while (i < 5) {
            System.out.println("i: " + i);
            i++;
        }
    }
}
```

### 3.4 Do-While

Vòng lặp do-while tương tự như vòng lặp while, nhưng điều kiện được kiểm tra sau khi khối lệnh đã được thực hiện ít nhất một lần. Điều này đảm bảo rằng khối lệnh sẽ được thực hiện ít nhất một lần, ngay cả khi điều kiện ban đầu là sai.

Cú pháp:

```java
do {
    // Khối lệnh sẽ được thực hiện ít nhất một lần
} while (condition);
```

Ví dụ:

```java
public class DoWhileLoopExample {
    public static void main(String[] args) {
        int i = 0;
        do {
            System.out.println("i: " + i);
            i++;
        } while (i < 5);
    }
}
```

### 3.5 For

Vòng lặp for là vòng lặp phổ biến nhất và thường được sử dụng khi bạn biết trước số lần lặp.

Vòng lặp for cung cấp một cấu trúc gọn gàng để khởi tạo biến, kiểm tra điều kiện, và cập nhật biến lặp tất cả trong một dòng.

Cú pháp:

```java
for (initialization; condition; update) {
    // Khối lệnh sẽ được thực hiện nếu condition là true
}
```

Ví dụ:

```
public class ForLoopExample {
    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            System.out.println("i: " + i);
        }
    }
}
```

### 3.6 break

Lệnh break trong Java là một công cụ mạnh mẽ để kiểm soát luồng thực thi của chương trình. 

Được sử dụng để:
- Kết thúc sớm một vòng lặp (for, while, hoặc do-while).
- Thoát khỏi một khối switch-case.

Sử dụng break trong vòng lặp:

```java
public class BreakInLoopExample {
    public static void main(String[] args) {
        int[] numbers = {1, 2, 3, 4, 5};
        int target = 3;

        for (int number : numbers) {
            if (number == target) {
                System.out.println("Found the target: " + number);
                break; // Thoát khỏi vòng lặp ngay khi tìm thấy giá trị cần tìm
            }
        }

        System.out.println("Loop ended.");
    }
}
```

Sử dụng break trong switch-case:

```java
public class BreakInSwitchExample {
    public static void main(String[] args) {
        int day = 3;
        String dayName;

        switch (day) {
            case 1:
                dayName = "Monday";
                break;
            case 2:
                dayName = "Tuesday";
                break;
            ...
        }

        System.out.println("The day is: " + dayName);
    }
}
```

### 3.7 continue

Lệnh continue trong Java được sử dụng để bỏ qua phần còn lại của vòng lặp hiện tại và chuyển ngay đến lần lặp tiếp theo. 

Được sử dụng trong các vòng lặp như for, while, và do-while.

```java
public class ContinueExample {
    public static void main(String[] args) {
        for (int i = 1; i <= 10; i++) {
            if (i % 2 == 0) {
                continue; // Bỏ qua các số chẵn
            }
            System.out.println(i); // In ra các số lẻ
        }
    }
}
```

=== THE END ===
